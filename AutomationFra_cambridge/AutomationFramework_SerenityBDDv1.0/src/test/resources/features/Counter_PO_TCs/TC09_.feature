@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_DropFlow_ErrorMsg_Barcode_Invalid_Bpost_Barcode

	@TEST_NRO-3520
	Scenario: Counter_DropFlow_ErrorMsg_Barcode_Invalid_Bpost_Barcode
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		And Validate wrong barcode in Drop "0105412885078321690001000013" scan with its error message "This barcode is not a bpost barcode and cannot be accepted"
