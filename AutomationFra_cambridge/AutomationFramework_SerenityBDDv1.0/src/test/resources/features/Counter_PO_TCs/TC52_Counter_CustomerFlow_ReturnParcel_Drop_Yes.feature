@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_CustomerFlow_Drop_ReturnParcel_Drop_Yes

	@TEST_NRO-4267
	Scenario: Counter_CustomerFlow_Drop_ReturnParcel_Drop_Yes
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		Then Select printers using printer configuration
		Then Enter the ReturnParcelPrint Yes barcode in Drop with its value "323299901059958190929134"
