@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_CustomerFlow_PickupFlow_Errormessage_This item is not yet present in your inventory. Please first do a scan of the item via “My Inventory - Intake”

	@TEST_NRO-3531
	Scenario: Counter_CustomerFlow_PickupFlow_Errormessage_This item is not yet present in your inventory. Please first do a scan of the item via “My Inventory - Intake”
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		And Validate wrong barcode in Pickup "010541288507831221690001000136" scan with its error message "This item is not yet present in your inventory. Please first do a scan of the item via “My Inventory - Intake”"
