@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_CustomerFlow_Drop_Pickup_Franking_Payment with Maestro

	@TEST_NRO-4056
	Scenario: Counter_CustomerFlow_Drop_Pickup_Franking_Payment with Maestro
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		Then Select printers using printer configuration
		Then Enter the barcode without checkout for Pickup with value "CE500452256BE"
		Then Enter the Drop barcode without login and add to cart with its value "CE500452295BE"
		Then Select franking item without login
