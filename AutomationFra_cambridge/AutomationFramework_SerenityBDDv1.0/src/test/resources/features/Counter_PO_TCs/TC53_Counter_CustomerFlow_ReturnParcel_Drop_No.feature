@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_CustomerFlow_Drop_ReturnParcel_Print_No

	@TEST_NRO-4268
	Scenario: Counter_CustomerFlow_Drop_ReturnParcel_Print_No
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		Then Select printers using printer configuration
		Then Enter the ReturnParcelPrint No barcode in Drop with its value "323299901059958190929134"
