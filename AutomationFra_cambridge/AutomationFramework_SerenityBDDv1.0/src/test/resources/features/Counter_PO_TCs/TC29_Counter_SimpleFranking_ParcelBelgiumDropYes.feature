@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_Franking_Parcel with Drop

	@TEST_NRO-4063 @franking
	Scenario: Counter_Franking_Parcel with Drop
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		Then Select printers using printer configuration
		Then Select franking items "Parcel Belgium - Drop Yes"
