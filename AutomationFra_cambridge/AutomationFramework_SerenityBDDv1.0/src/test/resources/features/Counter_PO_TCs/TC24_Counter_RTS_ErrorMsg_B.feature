@NRO-3434
Feature: Counter_XRAY Test Automation

Feature: Counter_ShopManagment_RTS_Errormessage_ItemNotFound

	@TEST_NRO-4049 @cambridge
	Scenario: Counter_ShopManagment_RTS_Errormessage_ItemNotFound
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		Then Validate wrong barcode in RTS "010541250783216901000013" scan with its error message "Item not found"
