@NRO-3434
Feature: Counter_XRAY Test Automation
//Feature: Counter_CustomerFlow_Drop_International Barcode with Checkin Inside EU

	@TEST_NRO-4115
	Scenario: Counter_CustomerFlow_Drop_International Barcode with Checkin Inside EU
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		Then Enter the International Checkin barcode Inside EU with its value "CI001291989AU"
		Then Enter the Receipient details like "ReceiverFirstname", "ReceiverLastname","28195","Bremen","address1","11","pranoti.panchal@tcs.com"
		Then Enter the CheckinSender details like "SenderFirstname" and "SenderLastname" and "1000 Brussel" and "Pachecalaan" and "10" and "var.surya@gmail.com" and "347656786786"
		Then Review Checkin details and Confirm Drop
