@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_PrinterNotConfigured_Errormsgvalidation

	@TEST_NRO-4086
	Scenario: Counter_PrinterNotConfigured_Errormsgvalidation
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		And Validate wrong barcode in Franking scan with its error message "Printer is not configured. Please go to Settings -> Device configuration and select the correct printer."
