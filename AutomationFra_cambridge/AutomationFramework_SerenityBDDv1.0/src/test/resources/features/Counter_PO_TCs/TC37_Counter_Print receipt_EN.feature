@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_CustomerFlow_Drop_Pickup_Domestic Barcode_Print Receipt_EN

	@TEST_NRO-4054
	Scenario: Counter_CustomerFlow_Drop_Pickup_Domestic Barcode_Print Receipt_EN
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		Then Select printers using printer configuration
		Then Enter the barcode without checkout for Pickup with value "CE500452313BE"
		Then Enter the barcode-Print receipt in EN without login with its value "CE500452300BE"
