@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_CustomerFlow_Drop_Pickup_Franking_Payment with Visa

	@TEST_NRO-4057
	Scenario: Counter_CustomerFlow_Drop_Pickup_Franking_Payment with Visa
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		Then Select printers using printer configuration
		Then Enter the barcode without checkout for Pickup with value "323215002174381009"
		Then Enter the Drop barcode without login and add to cart with its value "010541288507831221690001000150"
		Then Select franking item without login payment with Visa
