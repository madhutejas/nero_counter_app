@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_PickupFlow_Errormessage_Invalid_Bpost_Barcode

	@TEST_NRO-3530
	Scenario: Counter_PickupFlow_Errormessage_Invalid_Bpost_Barcode
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		And Validate wrong barcode in Pickup "01054128850783216901000013" scan with its error message "This barcode is not a bpost barcode and cannot be accepted"
