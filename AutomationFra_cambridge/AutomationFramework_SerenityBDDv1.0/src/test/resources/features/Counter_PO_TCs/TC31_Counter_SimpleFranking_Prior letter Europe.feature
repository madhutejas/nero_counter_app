@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_Franking_Prior letter_Europe

	@TEST_NRO-4065 @franking
	Scenario: Counter_Franking_Prior letter_Europe
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		Then Select printers using printer configuration
		Then Select franking items "Prior letter Europe"
