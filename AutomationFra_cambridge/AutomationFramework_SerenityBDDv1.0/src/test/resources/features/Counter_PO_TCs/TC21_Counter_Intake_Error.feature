@NRO-3434
Feature: Counter_XRAY Test Automation

//Feature: Counter_CustomerFlow_Intake_Errormessage_Invalid_Bpost_Barcode

	@TEST_NRO-3562
	Scenario: Counter_CustomerFlow_Intake_Errormessage_Invalid_Bpost_Barcode
		Given Launching the Application in "1"
		Then Login
		Then SelectorConfirm Location1
		Then VerifyHomePage
		And Validate wrong barcode in Intake "010541250783216901000013" scan with its error message "This barcode is not a bpost barcode and cannot be accepted"
