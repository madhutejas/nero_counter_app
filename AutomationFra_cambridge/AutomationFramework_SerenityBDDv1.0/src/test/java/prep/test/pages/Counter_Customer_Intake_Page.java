package prep.test.pages;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_Customer_Intake_Page extends PageObject {

	public Counter_Customer_Intake_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void IntakeBarcodeType(String IBarcodeType) throws Throwable {
		Thread.sleep(3000);

		AndroidElement ManageYourShop = driver
				.findElementByXPath("//android.widget.TextView[@text='Manage your \n" + "shop']");
		ManageYourShop.click();

		System.out.println("Inside Shop Management FLow");

		AndroidElement IntakeFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Intake']");
		IntakeFlowButton.click();
		System.out.println("Intake Flow");
		

		switch (IBarcodeType) {
		case "0":// for Small Parcel
			System.out.println("Selecting parcel type - Small Parcel");
			AndroidElement IntakeSmallParcel = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Small parcel']");
			IntakeSmallParcel.click();

			boolean IntakeSmallParcelPresence = driver
					.findElement(By.xpath("//android.widget.Button[@text='Continue']")).isDisplayed();
			boolean IntakeSmallParcelE = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"))
					.isEnabled();

			if (IntakeSmallParcelPresence == true && IntakeSmallParcelE == true) {
				// click on the confirm button
				WebElement ConfirmBut1 = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"));
				ConfirmBut1.click();
			}
			break;
		case "1":// for Big Parcel
			System.out.println("Selecting parcel type - Big Parcel");
			AndroidElement IntakeBigParcel = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Big parcel']");
			IntakeBigParcel.click();

			boolean IntakeBigParcelPresence = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"))
					.isDisplayed();
			boolean IntakeBigParcele = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"))
					.isEnabled();

			if (IntakeBigParcelPresence == true && IntakeBigParcele == true) {
				// click on the confirm button
				WebElement ConfirmBut1 = driver.findElement(By
						.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_next']"));
				ConfirmBut1.click();
			}
			break;
		case "2":// for Small Registered letter
			System.out.println("Selecting parcel type - Small Registered Letter");
			AndroidElement IntakeSmallRegLet = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Small registered letter']");
			IntakeSmallRegLet.click();

			boolean IntakeSmallRegLetPresence = driver
					.findElement(By.xpath("//android.widget.Button[@text='Continue']")).isDisplayed();
			boolean IntakeSmallRegLete = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"))
					.isEnabled();

			if (IntakeSmallRegLetPresence == true && IntakeSmallRegLete == true) {
				// click on the confirm button
				WebElement ConfirmBut1 = driver.findElement(By
						.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_next']"));
				ConfirmBut1.click();
			}
			break;
		case "3":// for Big Registered Letter
			System.out.println("Selecting parcel type - Big Registered Letter");
			AndroidElement IntakeBigRegLet = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Big registered letter']");
			IntakeBigRegLet.click();

			boolean IntakeBigRegLetPresence = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"))
					.isDisplayed();
			boolean IntakeBigRegLete = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"))
					.isEnabled();

			if (IntakeBigRegLetPresence == true && IntakeBigRegLete == true) {
				// click on the confirm button
				WebElement ConfirmBut1 = driver.findElement(By
						.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_next']"));
				ConfirmBut1.click();
			}
			break;
		}
	}

	public void ReIntakeBarcodeValue(String RIBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		// AndroidElement AllowIntake =
		// driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		// AllowIntake.click();
		
		AndroidElement AllowIntake = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowIntake.click();

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement IntakeBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		IntakeBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement IntakeBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		IntakeBarEditBox.click();
		IntakeBarEditBox.sendKeys(RIBarcodeValue);
		Thread.sleep(3000);

		AndroidElement IntakeSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		IntakeSubmitBut.click();

		boolean ReintakePresence = driver.findElement(By.xpath("//android.widget.Button[@text='Yes']")).isDisplayed();
		boolean Reintake = driver.findElement(By.xpath("//android.widget.Button[@text='Yes']")).isEnabled();

		if (ReintakePresence == true && Reintake == true) {
			// click on the confirm button
			WebElement Reintake1 = driver.findElement(By.xpath("//android.widget.Button[@text='Yes']"));
			Reintake1.click();
		}
		FileUtils.copyFile(file, new File("Barcode scanned in Pickup.jpg"));

	}

	public void IntakeBarcodeValue(String IBarcodeValue) throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		// AndroidElement AllowIntake =
		// driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		// AllowIntake.click();
		
		AndroidElement AllowIntake = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowIntake.click();

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement IntakeBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		IntakeBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement IntakeBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		IntakeBarEditBox.click();
		IntakeBarEditBox.sendKeys(IBarcodeValue);
		Thread.sleep(3000);
	}

	public void IntakeBarcodeValueSubmit() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		AndroidElement IntakeSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		IntakeSubmitBut.click();

		/*boolean ConfirmButPresence = driver
				.findElement(By.xpath(
						"//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"))
				.isDisplayed();
		boolean ConfirmBut = driver
				.findElement(By.xpath(
						"//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"))
				.isEnabled();

		if (ConfirmButPresence == true && ConfirmBut == true) {*/
			// click on the confirm button
		System.out.println("before intake confirm");
		AndroidElement ConfirmBut1 = driver.findElementByXPath(
				"//android.widget.Button[@text='Confirm (1)']");
		ConfirmBut1.click();
		System.out.println("after intake confirm");
			//WebElement ConfirmBut1 = driver.findElement(By
				//	.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"));
			//ConfirmBut1.click();
		//}
		/*
		 * else { System.out.println("Barcode not identified...Please check"); }
		 */
	}

	public void ValidateIntakeGreatjob() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		boolean GreatJobPresence = driver.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"))
				.isDisplayed();

		if (GreatJobPresence == true) {

			WebElement GreatJobPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"));
			System.out.println("Great Job page received");
		}

		FileUtils.copyFile(file, new File("Pickup Great Job page.jpg"));
	}

	public void ValidErrMessIntake(String IEBarcodeValue, String IEErrMsg) throws Throwable {

		AndroidElement ManageYourShop = driver
				.findElementByXPath("//android.widget.TextView[@text='Manage your \n" + "shop']");
		ManageYourShop.click();

		System.out.println("Inside Shop Management FLow");

		AndroidElement IntakeFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Intake']");
		IntakeFlowButton.click();
		System.out.println("Intake Flow");

		AndroidElement IntakeSmallParcel = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Small parcel']");
		IntakeSmallParcel.click();

		boolean IntakeSmallParcelPresence = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"))
				.isDisplayed();
		boolean IntakeSmallParcelE = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"))
				.isEnabled();

		if (IntakeSmallParcelPresence == true && IntakeSmallParcelE == true) {
			// click on the confirm button
			WebElement ConfirmBut1 = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"));
			ConfirmBut1.click();
		}

		// AndroidElement AllowIntake =
		// driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		// AllowIntake.click();
		
		AndroidElement AllowIntake = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowIntake.click();

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Barcode entering page loaded successfully.jpg"));
		AndroidElement IntakeBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		IntakeBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement IntakeBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		IntakeBarEditBox.click();
		IntakeBarEditBox.sendKeys(IEBarcodeValue);
		AndroidElement IntakeSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		IntakeSubmitBut.click();
		AndroidElement ErrPopUp = driver.findElementByXPath("//android.widget.TextView[@text='" + IEErrMsg + "']");
		Thread.sleep(5000);
		if (ErrPopUp.isDisplayed()) {
			FileUtils.copyFile(file, new File("Getting expected error message successfully.jpg"));
			System.out.println("Getting expected error message successfully");
		} else {
			FileUtils.copyFile(file, new File("Not Getting expected error message successfully after scan.jpg"));
			System.out.println("Not Getting expected error message successfully after scan");
		}
	}
}
