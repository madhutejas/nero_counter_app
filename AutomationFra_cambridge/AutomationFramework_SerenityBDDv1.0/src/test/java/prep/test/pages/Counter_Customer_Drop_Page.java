package prep.test.pages;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.kafka.common.utils.Exit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_Customer_Drop_Page extends PageObject {

	public Counter_Customer_Drop_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void DropDomesticBarcodeWithEmailPopup(String DBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowDrop = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowDrop.click();

		AndroidElement DropFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Send']");
		DropFlowButton.click();
		System.out.println("Drop Flow");

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement DropBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		DropBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement DropBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		DropBarEditBox.click();
		DropBarEditBox.sendKeys(DBarcodeValue);
		Thread.sleep(3000);
		AndroidElement DropSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		DropSubmitBut.click();

		FileUtils.copyFile(file, new File("Barcode scanned in Drop.jpg"));

	}

	public void DropPrintatofficeYes(String DBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowDrop = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowDrop.click();

		AndroidElement DropFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Send']");
		DropFlowButton.click();
		System.out.println("Drop Flow");

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement DropBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		DropBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement DropBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		DropBarEditBox.click();
		DropBarEditBox.sendKeys(DBarcodeValue);
		Thread.sleep(3000);

		AndroidElement DropSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		DropSubmitBut.click();

		AndroidElement PrintatofficeYes = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
		PrintatofficeYes.click();

		FileUtils.copyFile(file, new File("Barcode scanned in Drop.jpg"));
	}

	public void DropPrintatofficeNo(String DBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowDrop = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowDrop.click();

		AndroidElement DropFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Send']");
		DropFlowButton.click();
		System.out.println("Drop Flow");

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement DropBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		DropBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement DropBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		DropBarEditBox.click();
		DropBarEditBox.sendKeys(DBarcodeValue);
		Thread.sleep(3000);

		AndroidElement DropSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		DropSubmitBut.click();

		AndroidElement PrintatofficeNo = driver.findElementByXPath("//android.widget.Button[@text='No']");
		PrintatofficeNo.click();

		FileUtils.copyFile(file, new File("Barcode scanned in Drop.jpg"));
	}

	public void DropPrintatofficePrintererr(String DBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowDrop = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowDrop.click();

		AndroidElement DropFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Send']");
		DropFlowButton.click();
		System.out.println("Drop Flow");

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement DropBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		DropBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement DropBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		DropBarEditBox.click();
		DropBarEditBox.sendKeys(DBarcodeValue);
		Thread.sleep(3000);

		AndroidElement DropSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		DropSubmitBut.click();

		AndroidElement PrintatofficeYes = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
		PrintatofficeYes.click();

		AndroidElement ErrPopUp = driver
				.findElementByXPath("//android.widget.TextView[@text='Printer is not configured. \n"
						+ "Please go to Settings -> Device configuration and select the correct printer.']");
		Thread.sleep(5000);
		if (ErrPopUp.isDisplayed()) {
			FileUtils.copyFile(file, new File("Getting expected error message successfully.jpg"));
			System.out.println("Getting expected error message successfully");
		} else {
			FileUtils.copyFile(file, new File("Not Getting expected error message successfully after scan.jpg"));
			System.out.println("Not Getting expected error message successfully after scan");
		}

		FileUtils.copyFile(file, new File("Barcode scanned in Drop.jpg"));
	}

	public void DropDomesticBarcodeWithoutEmail(String DBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowDrop = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowDrop.click();

		AndroidElement DropFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Send']");
		DropFlowButton.click();
		System.out.println("Drop Flow");

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement DropBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		DropBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement DropBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		DropBarEditBox.click();
		DropBarEditBox.sendKeys(DBarcodeValue);
		Thread.sleep(3000);
		AndroidElement DropSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		DropSubmitBut.click();

		FileUtils.copyFile(file, new File("Barcode scanned in Drop.jpg"));

	}

	public void DropDomesticBarcodeWithoutlogin(String DBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement DropFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Send']");
		DropFlowButton.click();
		System.out.println("Drop Flow");

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement DropBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		DropBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement DropBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		DropBarEditBox.click();
		DropBarEditBox.sendKeys(DBarcodeValue);
		Thread.sleep(3000);
		AndroidElement DropSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		DropSubmitBut.click();

		FileUtils.copyFile(file, new File("Barcode scanned in Drop.jpg"));

	}

	public void DropBarcodeSubmit(String DBarcodeValue) throws Throwable {

		System.out.println("Inside DropBarcodeSubmit ");

		boolean ConfirmButPresence = driver.findElement(By.xpath("//android.widget.Button[@text='Confirm (1)']"))
				.isDisplayed();
		boolean ConfirmBut = driver.findElement(By.xpath("//android.widget.Button[@text='Confirm (1)']")).isEnabled();

		if (ConfirmButPresence == true && ConfirmBut == true) {
			// click on the search button
			WebElement ConfirmBut1 = driver.findElement(By.xpath("//android.widget.Button[@text='Confirm (1)']"));
			ConfirmBut1.click();
		}
	}

	public void Dropneedmoredetails(String DBarcodeValue) throws Throwable {

		System.out.println("Inside We need some more details to send the item ");

		AndroidElement Dropmoredetailsokbut = driver.findElementByXPath("//android.widget.Button[@text='OK']");
		Dropmoredetailsokbut.click();
	}

	public void DropShipmentDetailsinsideEU() throws Throwable {

		System.out.println("Inside Shipment Details");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		boolean ShipmentDetailsPresence = driver
				.findElement(By.xpath("//android.widget.TextView[@text='Shipment details']")).isDisplayed();
		boolean ShipmentDetails = driver.findElement(By.xpath("//android.widget.TextView[@text='Shipment details']"))
				.isEnabled();

		if (ShipmentDetailsPresence == true && ShipmentDetails == true) {
			// select country
			WebElement Countrylist = driver.findElement(By.xpath("//android.widget.EditText[@text='Search']"));
			Countrylist.click();
			Countrylist.sendKeys("germany");

			WebElement selectcountry = driver.findElement(By.xpath("//android.widget.Button[@text='Confirm drop']"));
			selectcountry.click();
			driver.hideKeyboard();
			//selectcountry.sendKeys(Keys.TAB);
			Thread.sleep(1000);

			// Add weight
			WebElement selectweight = driver
					.findElement(By.xpath("//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/tv_weight']"));
			selectweight.click();
			selectweight.sendKeys("120");
			driver.hideKeyboard();

			// Click on complete details

			WebElement completedetails = driver
					.findElement(By.xpath("//android.widget.TextView[@text='Complete details']"));
			completedetails.click();
			Thread.sleep(3000);
		}
		FileUtils.copyFile(file, new File("Shipment details.jpg"));
	}

	public void DropShipmentDetailsoutsideEU() throws Throwable {

		System.out.println("Inside Shipment Details");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		boolean ShipmentDetailsPresence = driver
				.findElement(By.xpath("//android.widget.TextView[@text='Shipment details']")).isDisplayed();
		boolean ShipmentDetails = driver.findElement(By.xpath("//android.widget.TextView[@text='Shipment details']"))
				.isEnabled();

		if (ShipmentDetailsPresence == true && ShipmentDetails == true) {
			// select country
			WebElement Countrylist = driver.findElement(By.xpath("//android.widget.EditText[@text='Search']"));
			Countrylist.click();
			Countrylist.sendKeys("india");

			WebElement selectcountry = driver.findElement(By.xpath("//android.widget.Button[@text='Confirm drop']"));
			selectcountry.click();
			driver.hideKeyboard();
			Thread.sleep(1000);

			// Click on complete details

			WebElement completedetails = driver
					.findElement(By.xpath("//android.widget.TextView[@text='Complete details']"));
			completedetails.click();
			Thread.sleep(3000);
		}
		FileUtils.copyFile(file, new File("Shipment details.jpg"));
	}

	public void Dropprohibitedcontent() throws Throwable {

		System.out.println("Inside Prohibited and restricted items");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// accept terms and conditions
		AndroidElement acceptterms = driver.findElementByXPath(
				"//android.widget.CheckBox[@resource-id ='be.bpost.nerocounterappnative.po:id/cb_agree_terms']");
		acceptterms.click();
		AndroidElement confirm = driver.findElementByXPath("//android.widget.Button[@text ='Confirm']");
		confirm.click();

		FileUtils.copyFile(file, new File("Inside Prohibited and restricted items.jpg"));
	}

	public void EnterReceipientDetailsinternational(String RecFirstName, String RecLastName, String RecPostalCode,
			String RecCity, String RecAdress, String streetnumber, String RecEmail) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Entering Receipient details.jpg"));

		if (!RecFirstName.isEmpty()) {
			AndroidElement RecFirstNameEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_first_name']");
			RecFirstNameEditBox.sendKeys(RecFirstName);
		}
		if (!RecLastName.isEmpty()) {
			AndroidElement RecLastNameEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_last_name']");
			RecLastNameEditBox.sendKeys(RecLastName);
			// RecLastNameEditBox.sendKeys(Keys.TAB);
			// RecLastNameEditBox.sendKeys(Keys.ENTER);
		}

		if (!RecPostalCode.isEmpty()) {
			AndroidElement RecPostalCodeEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_postalcode']");
			RecPostalCodeEditBox.sendKeys(RecPostalCode);
		}
		if (!RecCity.isEmpty()) {
			AndroidElement RecCityEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_city']");
			RecCityEditBox.sendKeys(RecCity);
		}
		if (!RecAdress.isEmpty()) {
			AndroidElement RecAdressEditBox1 = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_address1']");
			RecAdressEditBox1.sendKeys(RecAdress);
		}
		Thread.sleep(2000);
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"Street Number\"));");
		System.out.println("Scroll successfull");
		Thread.sleep(2000);

		if (!streetnumber.isEmpty()) {
			AndroidElement StreetEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_number']");
			StreetEditBox.sendKeys(RecEmail);
		}
		if (!RecEmail.isEmpty()) {
			AndroidElement RecEmailEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_email']");
			RecEmailEditBox.sendKeys(RecEmail);
		}

		AndroidElement receivercontinue = driver.findElementByXPath("//android.widget.Button[@text='Continue']");
		receivercontinue.click();
		FileUtils.copyFile(file, new File("Receipient Details Entered Successfully.jpg"));
	}

	public void EnterSenderCheckinDetails(String SendFirstName, String SendLastName, String SendPostalCode,
			String SendStreet, String SendStreetNum, String SendEmail, String SendPhoneNum) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Entering Sender details.jpg"));

		if (!SendFirstName.isEmpty()) {
			AndroidElement SendFirstNameEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/ti_input_first_name']");
			SendFirstNameEditBox.sendKeys(SendFirstName);
		}
		if (!SendLastName.isEmpty()) {
			AndroidElement SendLastNameEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/ti_input_last_name']");
			SendLastNameEditBox.sendKeys(SendLastName);
		}
		if (!SendPostalCode.isEmpty()) {
			AndroidElement SendPostalCodeEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/ti_input_postal_code']");
			String postcode = SendPostalCode;
			SendPostalCodeEditBox.click();
			SendPostalCodeEditBox.sendKeys("" + postcode.charAt(0));
			Thread.sleep(10000);
			driver.findElementByXPath("//android.widget.TextView[@text='Postal code and city']").click();
			Thread.sleep(3000);
		}
		if (!SendStreet.isEmpty()) {
			AndroidElement SendStreetEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/ti_input_street']");
			SendStreetEditBox.click();
			SendStreetEditBox.sendKeys("" + SendStreet.charAt(0));
			Thread.sleep(10000);
			driver.findElementByXPath("//android.widget.TextView[@text='Street']").click();
		}
		driver.hideKeyboard();
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"Street Number\"));");
		if (!SendStreetNum.isEmpty()) {
			AndroidElement SendStreetNumEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/ti_input_number']");
			SendStreetNumEditBox.sendKeys(SendStreetNum);
		}
		// driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new
		// UiSelector())"+".scrollIntoView(new UiSelector().text(\"Continue\"));"));
		if (!SendEmail.isEmpty()) {
			AndroidElement SendEmailEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/ti_input_email']");
			SendEmailEditBox.sendKeys(SendEmail);
		}
		if (!SendPhoneNum.isEmpty()) {
			AndroidElement SendPhoneNumEditBox = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/ti_input_phone']");
			SendPhoneNumEditBox.sendKeys(SendPhoneNum);
		}
		AndroidElement Sendercontinue = driver.findElementByXPath("//android.widget.Button[@text='Continue']");
		Sendercontinue.click();
		FileUtils.copyFile(file, new File("Sender Details Entered Successfully.jpg"));
	}

	public void DropShipemntTypeDocument() throws Throwable {

		System.out.println("Inside Shipment type selection");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		/*
		 * boolean ShipmentTypePresence = driver .findElement(By.
		 * xpath("//android.widget.TextView[@text='What's inside this parcel?']")).
		 * isDisplayed(); boolean ShipmentType = driver.findElement(By.
		 * xpath("//android.widget.TextView[@text='What's inside this parcel?']"))
		 * .isEnabled();
		 * 
		 * if (ShipmentTypePresence == true && ShipmentType == true) {
		 */
		// select Shipment Type
		// driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/img_down_arrow']").click();
		// driver.findElementByXPath("//android.widget.TextView[@text='Documents']").click();
		AndroidElement ShipmentTypeList = driver.findElement(By.xpath(
				"//android.view.ViewGroup[@resource-id='be.bpost.nerocounterappnative.po:id/cl_item_container']"));
		ShipmentTypeList.click();

		AndroidElement selectShipmentType = driver
				.findElement(By.xpath("//android.view.ViewGroup[@bounds='[306,349][636,388]']"));
		selectShipmentType.click();

		System.out.println("Shipment type selected");
		Thread.sleep(1000);

		AndroidElement ContentDesc = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_content_description']"));
		System.out.println("Element identified");
		driver.hideKeyboard();
		ContentDesc.sendKeys("Documents Shipment type selected by user");

		AndroidElement Shipmentweight = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_shipment_weight']"));
		Shipmentweight.sendKeys("120");
		driver.hideKeyboard();

		AndroidElement SubmitShiptype = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"));
		SubmitShiptype.click();
		// }
		FileUtils.copyFile(file, new File("Shipment details.jpg"));
	}

	public void DropShipmenttypeGift() throws Throwable {

		System.out.println("Inside Shipment type selection");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		
		// select country
		AndroidElement ShipmentTypeList = driver.findElement(By.xpath(
				"//android.view.ViewGroup[@resource-id='be.bpost.nerocounterappnative.po:id/cl_item_container']"));
		ShipmentTypeList.click();

		AndroidElement selectShipmentType = driver
				.findElement(By.xpath("//android.view.ViewGroup[@bounds='[306,388][636,427]']"));
		selectShipmentType.click();

		System.out.println("Shipment type selected");
		Thread.sleep(1000);

		AndroidElement ContentDesc = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_content_description']"));
		System.out.println("Element identified");
		driver.hideKeyboard();
		ContentDesc.sendKeys("Gifts Shipment type selected by user");
		driver.hideKeyboard();
		
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"Item description\"));");
		System.out.println("Scroll successfull");
		
		AndroidElement EditItem = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_item_description']"));
		driver.hideKeyboard();
		EditItem.sendKeys("Gift Item");
		driver.hideKeyboard();
		
		AndroidElement IncreaseItemnumber = driver.findElement(By.xpath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/img_add']"));
		IncreaseItemnumber.click();
		
		AndroidElement Shipmentweight = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_weight_of_the_items']"));
		Shipmentweight.sendKeys("120");
		driver.hideKeyboard();
		
		AndroidElement ItemValue = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_value_of_the_items']"));
		ItemValue.sendKeys("3");
		driver.hideKeyboard();

		AndroidElement SubmitShiptype = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"));
		SubmitShiptype.click();
		// }
		FileUtils.copyFile(file, new File("Shipment details.jpg"));
	}

	public void DropShipmenttypeSample() throws Throwable {

		System.out.println("Inside Shipment type selection");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		// select Shipment type
		WebElement ShipmentTypeList = driver.findElement(By.xpath(
				"//android.view.ViewGroup[@resource-id='be.bpost.nerocounterappnative.po:id/cl_item_container']"));
		ShipmentTypeList.click();

		WebElement selectShipmentType = driver
				.findElement(By.xpath("//android.widget.TextView[@bounds='[326,437][372,456]']"));
		selectShipmentType.click();

		System.out.println("Shipment type selected");
		Thread.sleep(1000);

		AndroidElement ContentDesc = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_content_description']"));
		System.out.println("Element identified");
		driver.hideKeyboard();
		ContentDesc.sendKeys("Sample Shipment type selected by user");
		driver.hideKeyboard();
		
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"Item description\"));");
		System.out.println("Scroll successfull");
		
		AndroidElement EditItem = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_item_description']"));
		driver.hideKeyboard();
		EditItem.sendKeys("Sample Item");
		driver.hideKeyboard();
		
		AndroidElement IncreaseItemnumber = driver.findElement(By.xpath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/img_add']"));
		IncreaseItemnumber.click();
		
		AndroidElement Shipmentweight = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_weight_of_the_items']"));
		Shipmentweight.sendKeys("120");
		driver.hideKeyboard();
		
		AndroidElement ItemValue = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_value_of_the_items']"));
		ItemValue.sendKeys("3");
		driver.hideKeyboard();

		AndroidElement SubmitShiptype = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"));
		SubmitShiptype.click();
		// }
		FileUtils.copyFile(file, new File("Shipment details.jpg"));
	}

	public void DropShipmenttypeRetGoods() throws Throwable {

		System.out.println("Inside Shipment type selection");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		// select shipment type
		WebElement ShipmentTypeList = driver.findElement(By.xpath(
				"//android.view.ViewGroup[@resource-id='be.bpost.nerocounterappnative.po:id/cl_item_container']"));
		ShipmentTypeList.click();

		WebElement selectShipmentType = driver
				.findElement(By.xpath("//android.widget.TextView[@bounds='[326,476][427,495]']"));
		selectShipmentType.click();

		System.out.println("Shipment type selected");
		Thread.sleep(1000);

		AndroidElement ContentDesc = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_content_description']"));
		System.out.println("Element identified");
		driver.hideKeyboard();
		ContentDesc.sendKeys("Returned Goods Shipment type selected by user");
		driver.hideKeyboard();
		
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"Item description\"));");
		System.out.println("Scroll successfull");
		
		AndroidElement EditItem = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_item_description']"));
		driver.hideKeyboard();
		EditItem.sendKeys("Returned Goods Item");
		driver.hideKeyboard();
		
		AndroidElement IncreaseItemnumber = driver.findElement(By.xpath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/img_add']"));
		IncreaseItemnumber.click();
		
		AndroidElement Shipmentweight = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_weight_of_the_items']"));
		Shipmentweight.sendKeys("120");
		driver.hideKeyboard();
		
		AndroidElement ItemValue = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_value_of_the_items']"));
		ItemValue.sendKeys("3");
		driver.hideKeyboard();
		
		AndroidElement CountryofOrigin = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/atv_input_country_origin']"));
		CountryofOrigin.click();
		
		AndroidElement SelCountryofOrigin = driver.findElement(By.xpath(
				"//android.widget.TextView[@resource-id='be.bpost.nerocounterappnative.po:id/tv_country_origin']"));
		SelCountryofOrigin.click();

		AndroidElement SubmitShiptype = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"));
		SubmitShiptype.click();
		
		FileUtils.copyFile(file, new File("Shipment details.jpg"));
	}

	public void DropShipmenttypeGoods() throws Throwable {

		System.out.println("Inside Shipment type selection");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		// select shipment type
		WebElement ShipmentTypeList = driver.findElement(By.xpath(
				"//android.view.ViewGroup[@resource-id='be.bpost.nerocounterappnative.po:id/cl_item_container']"));
		ShipmentTypeList.click();

		WebElement selectShipmentType = driver
				.findElement(By.xpath("//android.widget.TextView[@bounds='[326,515][367,534]']"));
		selectShipmentType.click();

		System.out.println("Shipment type selected");
		Thread.sleep(1000);

		AndroidElement ContentDesc = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_content_description']"));
		System.out.println("Element identified");
		driver.hideKeyboard();
		ContentDesc.sendKeys("Goods Shipment type selected by user");
		driver.hideKeyboard();
		
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"Item description\"));");
		System.out.println("Scroll successfull");
		
		AndroidElement EditItem = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_item_description']"));
		driver.hideKeyboard();
		EditItem.sendKeys("Goods Item");
		driver.hideKeyboard();
		
		AndroidElement IncreaseItemnumber = driver.findElement(By.xpath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/img_add']"));
		IncreaseItemnumber.click();
		
		AndroidElement Shipmentweight = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_weight_of_the_items']"));
		Shipmentweight.sendKeys("120");
		driver.hideKeyboard();
		
		AndroidElement ItemValue = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_value_of_the_items']"));
		ItemValue.sendKeys("3");
		driver.hideKeyboard();
		
		AndroidElement CountryofOrigin = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/atv_input_country_origin']"));
		CountryofOrigin.click();
		
		AndroidElement SelCountryofOrigin = driver.findElement(By.xpath(
				"//android.widget.TextView[@resource-id='be.bpost.nerocounterappnative.po:id/tv_country_origin']"));
		SelCountryofOrigin.click();

		AndroidElement SubmitShiptype = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"));
		SubmitShiptype.click();
		
		FileUtils.copyFile(file, new File("Shipment details.jpg"));

	}

	public void DropShipmenttypeOther() throws Throwable {

		System.out.println("Inside Shipment type selection");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		// select Shipment type
		WebElement ShipmentTypeList = driver.findElement(By.xpath(
				"//android.view.ViewGroup[@resource-id='be.bpost.nerocounterappnative.po:id/cl_item_container']"));
		ShipmentTypeList.click();

		WebElement selectShipmentType = driver
				.findElement(By.xpath("//android.widget.TextView[@bounds='[326,554][361,573]']"));
		selectShipmentType.click();

		System.out.println("Shipment type selected");
		Thread.sleep(1000);

		AndroidElement ContentDesc = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_content_description']"));
		System.out.println("Element identified");
		driver.hideKeyboard();
		ContentDesc.sendKeys("Other Shipment type selected by user");
		driver.hideKeyboard();
		
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"Item description\"));");
		System.out.println("Scroll successfull");
		
		AndroidElement EditItem = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_item_description']"));
		driver.hideKeyboard();
		EditItem.sendKeys("Other Item");
		driver.hideKeyboard();
		
		AndroidElement IncreaseItemnumber = driver.findElement(By.xpath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/img_add']"));
		IncreaseItemnumber.click();
		
		AndroidElement Shipmentweight = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_weight_of_the_items']"));
		Shipmentweight.sendKeys("120");
		driver.hideKeyboard();
		
		AndroidElement ItemValue = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/til_input_value_of_the_items']"));
		ItemValue.sendKeys("3");
		driver.hideKeyboard();
		
		AndroidElement CountryofOrigin = driver.findElement(By.xpath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/atv_input_country_origin']"));
		CountryofOrigin.click();
		
		AndroidElement SelCountryofOrigin = driver.findElement(By.xpath(
				"//android.widget.TextView[@resource-id='be.bpost.nerocounterappnative.po:id/tv_country_origin']"));
		SelCountryofOrigin.click();

		AndroidElement SubmitShiptype = driver.findElement(By.xpath("//android.widget.Button[@text='Continue']"));
		SubmitShiptype.click();
		
		FileUtils.copyFile(file, new File("Shipment details.jpg"));
	}

	public void DropReviewCheckin() throws Throwable {

		System.out.println("Inside Review Checkin");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(1000);
		AndroidElement ReviewContinue = driver.findElement(By.xpath("//android.widget.Button[@text ='Continue']"));
		ReviewContinue.click();
	}

	public void ConfirmDropCheckin() throws Throwable {

		System.out.println("Inside Confirm Drop");
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		AndroidElement ConfirmDropbutton = driver
				.findElement(By.xpath("//android.widget.Button[@text ='Confirm drop']"));
		ConfirmDropbutton.click();

		boolean GreatJobPresence = driver.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"))
				.isDisplayed();

		if (GreatJobPresence == true) {

			WebElement GreatJobPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"));
			System.out.println("Great Job page received");
		}

		FileUtils.copyFile(file, new File("Drop Great Job page.jpg"));
	}

	public void DropmailPopup() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		System.out.println("Inside DropmailPopup ");

		boolean EmailYesPresence = driver.findElement(By.xpath("//android.widget.Button[@text ='Yes']")).isDisplayed();
		boolean EmailYes = driver.findElement(By.xpath("//android.widget.Button[@text ='Yes']")).isEnabled();

		if (EmailYesPresence == true && EmailYes == true) {
			// click on the search button
			WebElement EmailYes1 = driver.findElement(By.xpath("//android.widget.Button[@text ='Yes']"));
			EmailYes1.click();
			AndroidElement Enteremailbutton = driver.findElementByXPath(
					"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_customer_email']");
			Enteremailbutton.sendKeys("pranoti.panchal.ext@bpost.be");
			driver.hideKeyboard();
			AndroidElement ConfirmEmail = driver.findElementByXPath("//android.widget.Button[@text ='Submit']");
			ConfirmEmail.click();
		}

	}

	public void DropAddmoreitem() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		boolean AddToCartPresence = driver.findElement(By.xpath("//android.widget.Button[@text='Add More Item']"))
				.isDisplayed();
		boolean AddToCart = driver.findElement(By.xpath("//android.widget.Button[@text='Add More Item']")).isEnabled();

		if (AddToCartPresence == true && AddToCart == true) {
			// click on the confirm button
			WebElement AddToCart1 = driver.findElement(By.xpath("//android.widget.Button[@text='Add More Item']"));
			AddToCart1.click();
		}
		FileUtils.copyFile(file, new File("Add more item.jpg"));
	}

	public void DropCustomerReceipt() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		System.out.println("Before finalizing the basket ");
		Thread.sleep(30000);
		AndroidElement FinalizeShoppingBasketbutton = driver
				.findElementByXPath("//android.widget.Button[@text ='Finalize shopping basket']");
		FinalizeShoppingBasketbutton.click();

		System.out.println("After finalizing the basket ");

		Thread.sleep(2000);

		boolean CustomerReceiptPresence = driver
				.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']")).isDisplayed();

		if (CustomerReceiptPresence == true) {

			WebElement CustomerReceiptPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']"));
			System.out.println("Customer Receipt page received");
			//AndroidElement DigitReceiptLang = driver.findElementByXPath("//android.widget.RadioButton[@text ='EN']");
			//DigitReceiptLang.click();
			
			// Selecting no print receipt

						AndroidElement SelectNoreceipt = driver
								.findElementByXPath("//android.widget.RadioButton[@text ='No receipt required']");
						SelectNoreceipt.click();
		}

		FileUtils.copyFile(file, new File("Customer Receipt page.jpg"));
	}

	public void DropCustomerPrintReceipt() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		System.out.println("Before finalizing the basket");
		Thread.sleep(10000);
		AndroidElement FinalizeShoppingBasketbutton = driver
				.findElementByXPath("//android.widget.Button[@text ='Finalize shopping basket']");
		FinalizeShoppingBasketbutton.click();

		boolean CustomerReceiptPresence = driver
				.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']")).isDisplayed();

		if (CustomerReceiptPresence == true) {

			WebElement CustomerReceiptPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']"));
			System.out.println("Customer Receipt page received");

			// Selecting print receipt

			AndroidElement SelectPrinterReceipt = driver
					.findElementByXPath("//android.widget.RadioButton[@text ='Print receipt']");
			SelectPrinterReceipt.click();

			AndroidElement PrintReceiptLang = driver.findElementByXPath("//android.widget.RadioButton[@text ='EN']");
			PrintReceiptLang.click();
		}
	}

	public void DropNoCustomerReceipt() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		System.out.println("Before finalizing the basket");
		Thread.sleep(10000);
		AndroidElement FinalizeShoppingBasketbutton = driver
				.findElementByXPath("//android.widget.Button[@text ='Finalize shopping basket']");
		FinalizeShoppingBasketbutton.click();

		boolean CustomerReceiptPresence = driver
				.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']")).isDisplayed();

		if (CustomerReceiptPresence == true) {

			WebElement CustomerReceiptPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']"));
			System.out.println("Customer Receipt page received");

			// Selecting no print receipt

			AndroidElement SelectNoreceipt = driver
					.findElementByXPath("//android.widget.RadioButton[@text ='No receipt required']");
			SelectNoreceipt.click();
		}

		FileUtils.copyFile(file, new File("Customer Receipt page.jpg"));
	}

	public void DropBarcodeGreatJob(String DBarcodeValue) throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		AndroidElement SubmitCustReceipt = driver.findElementByXPath("//android.widget.Button[@text ='Submit']");
		SubmitCustReceipt.click();

		boolean GreatJobPresence = driver.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"))
				.isDisplayed();

		if (GreatJobPresence == true) {

			WebElement GreatJobPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"));
			System.out.println("Great Job page received");
		}

		FileUtils.copyFile(file, new File("Drop Great Job page.jpg"));
	}

	public void ValidErrMessDrop(String DEBarcodeValue, String DEErrMsg) throws Throwable {

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowDrop = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowDrop.click();

		AndroidElement DropFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Send']");
		DropFlowButton.click();
		System.out.println("Drop Flow");

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Barcode entering page loaded successfully.jpg"));
		AndroidElement DropBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		DropBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement DropBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		DropBarEditBox.click();
		DropBarEditBox.sendKeys(DEBarcodeValue);
		AndroidElement DropSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		DropSubmitBut.click();
		AndroidElement ErrPopUp = driver.findElementByXPath("//android.widget.TextView[@text='" + DEErrMsg + "']");
		Thread.sleep(5000);
		if (ErrPopUp.isDisplayed()) {
			FileUtils.copyFile(file, new File("Getting expected error message successfully.jpg"));
			System.out.println("Getting expected error message successfully");
		} else {
			FileUtils.copyFile(file, new File("Not Getting expected error message successfully after scan.jpg"));
			System.out.println("Not Getting expected error message successfully after scan");
		}
	}

	private void Exit() {
		// TODO Auto-generated method stub

	}
}
