package prep.test.pages;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_Customer_Pickup_Page extends PageObject {

	public Counter_Customer_Pickup_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void PickupBarcodeAddMoreItem(String PBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowPickup = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowPickup.click();

		AndroidElement PickupFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Pickup']");
		PickupFlowButton.click();
		System.out.println("Pickup Flow");
		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement PickupBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		PickupBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement PickupBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		PickupBarEditBox.click();
		PickupBarEditBox.sendKeys(PBarcodeValue);
		Thread.sleep(3000);
		AndroidElement PickupSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		PickupSubmitBut.click();

		Thread.sleep(3000);

		if (driver
				.findElement(By
						.xpath("//android.widget.TextView[@text='There are more items for the \n" + "same customer']"))
				.isDisplayed()
				&& driver
						.findElement(By.xpath(
								"//android.widget.TextView[@text='There are more items for the \n" + "same customer']"))
						.isEnabled()) {
			WebElement MoreItemsSameCustomer1 = driver.findElement(By.xpath(
					"//android.widget.CheckBox[@resource-id='be.bpost.nerocounterappnative.po:id/suggestion_checkbox']"));
			MoreItemsSameCustomer1.click();

			WebElement AddToList = driver.findElement(By.xpath(
					"//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_add_to_list']"));
			AddToList.click();
		}
	}

	public void PickupBarcodeAlreadyDelivered(String PBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowPickup = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowPickup.click();

		AndroidElement PickupFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Pickup']");
		PickupFlowButton.click();
		System.out.println("Pickup Flow");

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement PickupBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		PickupBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement PickupBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		PickupBarEditBox.click();
		PickupBarEditBox.sendKeys(PBarcodeValue);
		Thread.sleep(3000);
		AndroidElement PickupSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		PickupSubmitBut.click();

		if (driver.findElement(By.xpath("//android.widget.Button[@text='Deliver']")).isDisplayed()
				&& driver.findElement(By.xpath("//android.widget.Button[@text='Deliver']")).isEnabled()) {
			WebElement Deliveragain1 = driver.findElement(
					By.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_yes']"));
			Deliveragain1.click();

		}

	}

	public void PickupBarcodeScan(String PBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowPickup = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowPickup.click();

		AndroidElement PickupFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Pickup']");
		PickupFlowButton.click();
		System.out.println("Pickup Flow");

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement PickupBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		PickupBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement PickupBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		PickupBarEditBox.click();
		PickupBarEditBox.sendKeys(PBarcodeValue);
		Thread.sleep(3000);
		AndroidElement PickupSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		PickupSubmitBut.click();

	}

	public void PickupBarcodeScanwithaddmore(String PBarcodeValue) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowPickup = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowPickup.click();

		AndroidElement PickupFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Pickup']");
		PickupFlowButton.click();
		System.out.println("Pickup Flow");

		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement PickupBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		PickupBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement PickupBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		PickupBarEditBox.click();
		PickupBarEditBox.sendKeys(PBarcodeValue);
		Thread.sleep(3000);
		AndroidElement PickupSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		PickupSubmitBut.click();
		Thread.sleep(3000);
		boolean ConfirmButPresence = driver
				.findElement(By.xpath(
						"//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"))
				.isDisplayed();
		boolean ConfirmBut = driver
				.findElement(By.xpath(
						"//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"))
				.isEnabled();

		if (ConfirmButPresence == true && ConfirmBut == true) {
			// click on the confirm button
			WebElement ConfirmBut1 = driver.findElement(By
					.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"));
			ConfirmBut1.click();
		} else {
			System.out.println("Barcode not identified...Please check");
		}

		/*
		 * boolean AddToCartPresence =
		 * driver.findElement(By.xpath("//android.widget.Button[@text='Add to cart']"))
		 * .isDisplayed(); boolean AddToCart =
		 * driver.findElement(By.xpath("//android.widget.Button[@text='Add to cart']")).
		 * isEnabled();
		 * 
		 * if (AddToCartPresence == true && AddToCart == true) { // click on the confirm
		 * button WebElement AddToCart1 =
		 * driver.findElement(By.xpath("//android.widget.Button[@text='Add to cart']"));
		 * AddToCart1.click(); }
		 */

		AndroidElement PickupAddMore = driver.findElementByXPath("//android.widget.Button[@text='Add More Item']");
		PickupAddMore.click();

	}

	public void PickupBarcodeSubmit(String PBarcodeValue) throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file, new File("Barcode scanned in Pickup.jpg"));

		boolean ConfirmButPresence = driver
				.findElement(By.xpath(
						"//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"))
				.isDisplayed();
		boolean ConfirmBut = driver
				.findElement(By.xpath(
						"//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"))
				.isEnabled();

		if (ConfirmButPresence == true && ConfirmBut == true) {
			// click on the confirm button
			WebElement ConfirmBut1 = driver.findElement(By
					.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"));
			ConfirmBut1.click();
		} else {
			System.out.println("Barcode not identified...Please check");
		}

		boolean PictureIDPresence = driver
				.findElement(By.xpath("//android.widget.TextView[@text ='Take the picture of the ID card']"))
				.isDisplayed();
		// boolean PictureID =
		// driver.findElement(By.xpath("//android.widget.Button[@text
		// ='Yes']")).isEnabled();

		if (PictureIDPresence == true) {
			// click on the take picture
			WebElement PictureIDClick = driver.findElement(By.xpath(
					"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_capture']"));
			PictureIDClick.click();
			WebElement PictureIDOk = driver.findElement(
					By.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_ok']"));
			PictureIDOk.click();
		}

		boolean CustomSign = driver.findElement(By.xpath("//android.widget.TextView[@text ='Customer signature']"))
				.isDisplayed();

		if (CustomSign == true) {
			// click on the take picture
			WebElement CustomSign1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Customer signature']"));
			CustomSign1.click();
			Actions builder = new Actions(driver);
			WebElement canvasElement = driver.findElement(
					By.xpath("//android.view.View[@resource-id='be.bpost.nerocounterappnative.po:id/signatureView']"));
			// Action mouseoverSign = builder.moveToElement(canvasElement).build();

			Action signature = builder.clickAndHold(canvasElement).moveToElement(canvasElement, 20, -7)
					.moveByOffset(50, 50).moveByOffset(80, -50).moveByOffset(1, 5).release(canvasElement).build();
			signature.perform();
		}

		boolean AddToCartPresence = driver.findElement(By.xpath("//android.widget.Button[@text='Add to cart']"))
				.isDisplayed();
		boolean AddToCart = driver.findElement(By.xpath("//android.widget.Button[@text='Add to cart']")).isEnabled();

		if (AddToCartPresence == true && AddToCart == true) {
			// click on the confirm button
			WebElement AddToCart1 = driver.findElement(By.xpath("//android.widget.Button[@text='Add to cart']"));
			AddToCart1.click();
		}
	}

	public void PickupCustomerReceipt() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(20000);
		System.out.println("Before finalizing the shopping basket");
		AndroidElement FinalizeShoppingBasketbutton = driver
				.findElementByXPath("//android.widget.Button[@text ='Finalize shopping basket']");
		FinalizeShoppingBasketbutton.click();

		boolean CustomerReceiptPresence = driver
				.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']")).isDisplayed();

		if (CustomerReceiptPresence == true) {

			WebElement CustomerReceiptPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']"));
			System.out.println("Customer Receipt page received");
			// AndroidElement DigitReceiptLang =
			// driver.findElementByXPath("//android.widget.RadioButton[@text ='EN']");
			// DigitReceiptLang.click();
			// Selecting no print receipt

			AndroidElement SelectNoreceipt = driver
					.findElementByXPath("//android.widget.RadioButton[@text ='No receipt required']");
			SelectNoreceipt.click();
		}
		FileUtils.copyFile(file, new File("Customer Receipt page.jpg"));
	}

	public void PickupBarcodeGreatJob(String PBarcodeValue) throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		AndroidElement SubmitCustReceipt = driver.findElementByXPath("//android.widget.Button[@text ='Submit']");
		SubmitCustReceipt.click();

		boolean GreatJobPresence = driver.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"))
				.isDisplayed();

		if (GreatJobPresence == true) {

			WebElement GreatJobPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"));
			System.out.println("Great Job page received");
		}

		FileUtils.copyFile(file, new File("Pickup Great Job page.jpg"));
	}

	public void ValidErrMessPickup(String PEBarcodeValue, String PEErrMsg) throws Throwable {

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");

		AndroidElement AllowPickup = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowPickup.click();

		AndroidElement PickupFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Pickup']");
		PickupFlowButton.click();
		System.out.println("Pickup Flow");

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Barcode entering page loaded successfully.jpg"));
		AndroidElement PickupBarManualEntry = driver
				.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		PickupBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement PickupBarEditBox = driver.findElementByXPath(
				"//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		PickupBarEditBox.click();
		PickupBarEditBox.sendKeys(PEBarcodeValue);
		AndroidElement PickupSubmitBut = driver.findElementByXPath(
				"//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		PickupSubmitBut.click();
		AndroidElement ErrPopUp = driver.findElementByXPath("//android.widget.TextView[@text='" + PEErrMsg + "']");
		Thread.sleep(5000);
		if (ErrPopUp.isDisplayed()) {
			FileUtils.copyFile(file, new File("Getting expected error message successfully.jpg"));
			System.out.println("Getting expected error message successfully");
		} else {
			FileUtils.copyFile(file, new File("Not Getting expected error message successfully after scan.jpg"));
			System.out.println("Not Getting expected error message successfully after scan");
		}
	}
}
