package prep.test.pages;

import java.io.File;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;


public class EnterShipmentDetails_Page extends PageObject {

	public EnterShipmentDetails_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
	}
	
	public void SelectDestination(String DestCountry) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Shipment details selecting destination country.jpg"));
		AndroidElement SelDestCountry = driver.findElementByXPath("//android.widget.EditText[@text='Search']");
		SelDestCountry.click();
		SelDestCountry.sendKeys(DestCountry);
		FileUtils.copyFile(file, new File("Selecting destination country.jpg"));
		
		//handled country selection
		driver.findElementByXPath("//android.widget.TextView[@resource-id='be.bpost.preparationapp:id/tv_heading_shipment']").click();
		AndroidElement ShipDeta = driver.findElementByXPath("//android.widget.ImageButton[@resource-id='be.bpost.preparationapp:id/text_input_end_icon']");
		ShipDeta.click();
		Thread.sleep(1000);
		driver.findElementByXPath("//android.view.View[@resource-id='be.bpost.preparationapp:id/separator']").click();
		
		Thread.sleep(1000);
		AndroidElement CompleteDetails = driver.findElementByXPath("//android.widget.TextView[@text='Complete details']");
		CompleteDetails.click();
	}
	
	public void SelectDestInOROutEU(String DestCountryInOROutEU) throws Throwable {
		if(DestCountryInOROutEU.equalsIgnoreCase("InEU")) {
			Thread.sleep(1000);
			AndroidElement WeightOfShip = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/ted_weight']");
			WeightOfShip.click();
			WeightOfShip.sendKeys("300");
			driver.findElementByXPath("//android.widget.TextView[@text='Declared value international']").click();
			Thread.sleep(1000);
			AndroidElement CompleteDetails = driver.findElementByXPath("//android.widget.TextView[@text='Complete details']");
			CompleteDetails.click();
		}
	}
	
	public void AgreeProhibitedAndRestrictedItems() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(8000);
		FileUtils.copyFile(file, new File("Prohibited and restricted items page loaded successfully.jpg"));
		AndroidElement PrhibitedChkBox = driver.findElementByXPath("//android.widget.CheckBox[@resource-id='be.bpost.preparationapp:id/checkBox_declaration']");
		PrhibitedChkBox.click();
		Thread.sleep(1000);
		driver.findElementByXPath("//android.widget.Button[@text='Confirm']").click();
	}
	
	public void EnterReceipientDetails(String RecFirstName,String RecLastName, String RecPostalCode, String RecCity, String RecAdress, String RecEmail, String RecPhoneNum) throws Throwable{
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Entering Receipient details.jpg"));
		
		if(!RecFirstName.isEmpty())
		{
			AndroidElement RecFirstNameEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_first_name']");
			RecFirstNameEditBox.sendKeys(RecFirstName);
		}
		if(!RecLastName.isEmpty())
		{
			AndroidElement RecLastNameEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_last_name']");
			RecLastNameEditBox.sendKeys(RecLastName);
		}
		if(!RecPostalCode.isEmpty())
		{
			AndroidElement RecPostalCodeEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_postalcode']");
			RecPostalCodeEditBox.sendKeys(RecPostalCode);
		}
		if(!RecCity.isEmpty())
		{
			AndroidElement RecCityEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_city']");
			RecCityEditBox.sendKeys(RecCity);
		}
		if(!RecAdress.isEmpty())
		{
			AndroidElement RecAdressEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_address1']");
			RecAdressEditBox.sendKeys(RecAdress);
		}
		driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector())"+".scrollIntoView(new UiSelector().text(\"Continue\"));"));
		if(!RecEmail.isEmpty())
		{
			AndroidElement RecEmailEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_email']");
			RecEmailEditBox.sendKeys(RecEmail);
		}
		if(!RecPhoneNum.isEmpty())
		{
			AndroidElement RecPhoneNumEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_phone_num']");
			RecPhoneNumEditBox.sendKeys(RecPhoneNum);
		}
		FileUtils.copyFile(file, new File("Receipient Details Entered Successfully.jpg"));
	}
	
	public void EnterSenderDetails(String SendFirstName,String SendLastName, String SendPostalCode, String SendStreet, String SendStreetNum, String SendEmail, String SendPhoneNum) throws Throwable{
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Entering Sender details.jpg"));
		
		if(!SendFirstName.isEmpty())
		{
			AndroidElement SendFirstNameEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_first_name']");
			SendFirstNameEditBox.sendKeys(SendFirstName);
		}
		if(!SendLastName.isEmpty())
		{
			AndroidElement SendLastNameEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_last_name']");
			SendLastNameEditBox.sendKeys(SendLastName);
		}
		if(!SendPostalCode.isEmpty())
		{
			AndroidElement SendPostalCodeEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_postalcode']");
			String postcode = SendPostalCode;
			SendPostalCodeEditBox.click();
			SendPostalCodeEditBox.sendKeys(""+postcode.charAt(0));
			Thread.sleep(10000);
			driver.findElementByXPath("//android.widget.TextView[@text='Postal code and city']").click();
			Thread.sleep(3000);
//			for(int i=0;i<postcode.length();i++) {
//				SendPostalCodeEditBox.sendKeys(""+postcode.charAt(i));
//				Thread.sleep(20000);
//				SendPostalCodeEditBox.click();
//			}
//			TouchAction a2 = new TouchAction(driver);
//			a2.press(PointOption.point(SendPostalCodeEditBox.getLocation().getX() + 100,
//					SendPostalCodeEditBox.getLocation().getY() - 200)).release().perform();
		}
		if(!SendStreet.isEmpty())
		{
			AndroidElement SendStreetEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_street']");
			SendStreetEditBox.click();
			SendStreetEditBox.sendKeys(""+SendStreet.charAt(0));
			Thread.sleep(10000);
			driver.findElementByXPath("//android.widget.TextView[@text='Street']").click();
//			Thread.sleep(20000);
//			driver.getKeyboard().sendKeys(Keys.ARROW_RIGHT);
//			Thread.sleep(1000);
//			driver.getKeyboard().sendKeys(Keys.ENTER);
////			int x = SendStreetEditBox.getLocation().getX();
////			int y = SendStreetEditBox.getLocation().getY();
////			Thread.sleep(2000);
////			
////			TouchAction action = new TouchAction(driver).tap(x+60,y+260).release();
////			action.perform();
		}
		if(!SendStreetNum.isEmpty())
		{
			AndroidElement SendStreetNumEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_number']");
			SendStreetNumEditBox.sendKeys(SendStreetNum);
		}
		driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector())"+".scrollIntoView(new UiSelector().text(\"Continue\"));"));
		if(!SendEmail.isEmpty())
		{
			AndroidElement SendEmailEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_email']");
			SendEmailEditBox.sendKeys(SendEmail);
		}
		if(!SendPhoneNum.isEmpty())
		{
			AndroidElement SendPhoneNumEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_phone_num']");
			SendPhoneNumEditBox.sendKeys(SendPhoneNum);
		}
		FileUtils.copyFile(file, new File("Sender Details Entered Successfully.jpg"));
	}
	
	public void ValidateCompleteDetailsLink() throws Throwable {
		Thread.sleep(1000);
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
		AndroidElement CompleteDetails = driver.findElementByXPath("//android.widget.TextView[@text='Complete details']");
		if(CompleteDetails.isEnabled()==false) {
			FileUtils.copyFile(file, new File("Complete details link is disabled as expected.jpg"));
			System.out.println("Complete details link is disabled as expected");
		}
		else
		{
			FileUtils.copyFile(file, new File("Complete details is not disabled.jpg"));
			System.out.println("Complete details is not disabled");
		}
	}
	
	public void ValidateContinueButtonDisabled() throws Throwable {
		Thread.sleep(1000);
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
		AndroidElement ContinueBut = driver.findElementByXPath("//android.widget.Button[@text='Continue']");
		if(ContinueBut.isEnabled()==false) {
			FileUtils.copyFile(file, new File("ContinueBut is disabled as expected.jpg"));
			System.out.println("ContinueBut is disabled as expected");
		}
		else
		{
			FileUtils.copyFile(file, new File("ContinueBut is not disabled.jpg"));
			System.out.println("ContinueBut is not disabled");
		}
	}
	
	public void ClickContinue() throws Throwable {
		Thread.sleep(3000);
		driver.findElementByXPath("//android.widget.Button[@text='Continue']").click();
	}
	
	public void ValidateEmailValMsg() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		System.out.println("Validate email validation msg");
		AndroidElement EmailValMsg = driver.findElementByXPath("//android.widget.TextView[@text='Please enter a valid email address']");
		System.out.println(EmailValMsg.getAttribute("text"));
		if(EmailValMsg.isDisplayed()) {
			FileUtils.copyFile(file, new File("Getting expected message successfully.jpg"));
			System.out.println("Getting expected message successfully");
		}
		else
		{
			FileUtils.copyFile(file, new File("Not Getting expected message successfully.jpg"));
			System.out.println("Not Getting expected message successfully");
		}
	}
	
	public void ClickHome() throws Throwable {
		Thread.sleep(3000);
		driver.findElementByXPath("//android.widget.TextView[@content-desc='Home']").click();
	}
	
	public void SelectShipType(String shipmentTypeIndex) throws Throwable{
		Thread.sleep(3000);
		switch(shipmentTypeIndex){
			case "0"://for Documents
				System.out.println("Entering switch case for selecting shipment type");
				driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.preparationapp:id/img_down_arrow']").click();
				driver.findElementByXPath("//android.widget.TextView[@text='Documents']").click();
				//driver.findElementByXPath("//android.widget.TextView[@text='Documents']").click();
				Thread.sleep(1000);
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_content_description']").sendKeys("Overall Content description");
				Thread.sleep(1000);
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_shipment_weight']").sendKeys("500");
				break;
			case "1"://for Gifts
				driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.preparationapp:id/img_down_arrow']").click();
				driver.findElementByXPath("//android.widget.TextView[@text='Gifts']").click();
				Thread.sleep(1000);
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_content_description']").sendKeys("Overall Content description");
				driver.findElementByXPath("//android.widget.TextView[@text='What's inside this parcel?']").click();
				driver.findElementByXPath("//android.widget.TextView[@text='What's inside this parcel?']").sendKeys(Keys.TAB);
				//driver.findElement(MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector())"+".scrollIntoView(new UiSelector().textContains(\"Weight of the item\"));"));
				
				//Item description
				//driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_item_description']").click();
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_item_description']").sendKeys("Item description");
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_item_description']").sendKeys(Keys.TAB);
				//driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_number_items']").click();
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_number_items']").sendKeys("1");	
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_number_items']").sendKeys(Keys.TAB);
				//driver.findElementByXPath("//android.widget.TextView[@text='Item 1']").click();
				
				//driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_weight_of_the_items']").click();
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_weight_of_the_items']").sendKeys("300");	
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_weight_of_the_items']").sendKeys(Keys.TAB);
				//driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_value_of_the_items']").click();
				driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_value_of_the_items']").sendKeys("10");
				driver.findElementByXPath("//android.widget.TextView[@text='Item 1']").click();
				break;
			case "2"://for Sample
				driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.preparationapp:id/img_down_arrow']").click();
				driver.findElementByXPath("//android.widget.TextView[@text='Sample']").click();
				break;
			case "3"://for Returned Goods
				driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.preparationapp:id/img_down_arrow']").click();
				driver.findElementByXPath("//android.widget.TextView[@text='Returned Goods']").click();
				break;
			case "4"://for Goods
				driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.preparationapp:id/img_down_arrow']").click();
				driver.findElementByXPath("//android.widget.TextView[@text='Goods']").click();
				break;
			case "5"://for Other
				driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.preparationapp:id/img_down_arrow']").click();
				driver.findElementByXPath("//android.widget.TextView[@text='Other']").click();
				AndroidElement itemdesc = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_item_description']");
				
				JavascriptExecutor js = (JavascriptExecutor) driver;
				HashMap<String, String> scrollObject = new HashMap<String, String>();
				scrollObject.put("direction", "down");
				js.executeScript("mobile: swipe", scrollObject);
				
				itemdesc.sendKeys("items ");
				break;
		}
	}
	
	public void ValidatePopUp(String PopUpMsg) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		System.out.println("Getting the popup after clicking Home");
		FileUtils.copyFile(file, new File("Barcode entering page loaded successfully.jpg"));
		AndroidElement PopUpMsgBox = driver.findElementByXPath("//android.widget.Button[@resource-id='be.bpost.preparationapp:id/btn_home']");
		Thread.sleep(1000);
		System.out.println("Getting the popup after clicking Home");
		if(PopUpMsgBox.isDisplayed())
		{
			FileUtils.copyFile(file, new File("Getting expected popup message successfully.jpg"));
			System.out.println("Getting expected error message successfully");
		}
		
	}
	
	public void ValidateAdresChar() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		System.out.println("Validating adress character");
		FileUtils.copyFile(file, new File("Barcode entering page loaded successfully.jpg"));
		AndroidElement RecAdressEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_address1']");
		RecAdressEditBox.sendKeys("ReceAddressdddddddddttttttttttjjjjjjjjj");
		driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/til_input_country']").click();
		Thread.sleep(1000);
		AndroidElement AdressChar = driver.findElementByXPath("//android.widget.TextView[@resource-id='be.bpost.preparationapp:id/textinput_counter']");
		if(AdressChar.isDisplayed())
		{
			FileUtils.copyFile(file, new File("Characters are taking 40 only as expected.jpg"));
			System.out.println("Characters are taking 40 only as expected");
		}
		
	}

}
