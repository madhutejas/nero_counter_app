package prep.test.pages;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_Customer_RTS_Page extends PageObject{
	
	public Counter_Customer_RTS_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void RTSBarcodescan(String RBarcodeValue) throws Throwable{
		Thread.sleep(3000);
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		AndroidElement ManageYourShop = driver.findElementByXPath("//android.widget.TextView[@text='Manage your \n"
				+ "shop']");
		ManageYourShop.click();
		
		System.out.println("Inside Shop Management FLow");
		
		AndroidElement RTSFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Return to \n"
				+ " sender']");
		RTSFlowButton.click();
		System.out.println("RTS Flow");
		
		AndroidElement AllowRTS = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowRTS.click();
		
		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement RTSBarManualEntry = driver.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		RTSBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement RTSBarEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		RTSBarEditBox.click();
		RTSBarEditBox.sendKeys(RBarcodeValue);
		Thread.sleep(3000);
		
		AndroidElement RTSSubmitBut = driver.findElementByXPath("//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		RTSSubmitBut.click();
		
		boolean ConfirmButPresence = driver.findElement(By.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']")).isDisplayed();
		boolean ConfirmBut = driver.findElement(By.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']")).isEnabled();
		
		if (ConfirmButPresence==true && ConfirmBut==true)
        {
               // click on the confirm button
               WebElement ConfirmBut1 = driver.findElement(By.xpath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']"));
               ConfirmBut1.click();
        }
		Thread.sleep(3000);
		/*else
		{
			System.out.println("Barcode not identified...Please check");
		}
		*/
		
}
	
	public void RTSMissingSkip() throws Throwable{
		Thread.sleep(3000);
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		boolean MissingParcelsPresence = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='be.bpost.nerocounterappnative.po:id/tv_great_job']")).isDisplayed();
		boolean MissingParcelsBut = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='be.bpost.nerocounterappnative.po:id/tv_great_job']")).isEnabled();
		
	if (MissingParcelsPresence==true && MissingParcelsBut==true)
        {
               // click on the confirm button
               WebElement SkipMissing = driver.findElementByXPath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_next']");
               SkipMissing.click();
        }
		//AndroidElement SkipMissing = driver.findElementByXPath("//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_next']");
		//SkipMissing.click();
		
	}
	
	public void RTSMarkMissing() throws Throwable{
		Thread.sleep(3000);
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		boolean MissingParcelsPresence1 = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='be.bpost.nerocounterappnative.po:id/tv_great_job']")).isDisplayed();
		boolean MissingParcelsBut1 = driver.findElement(By.xpath("//android.widget.TextView[@resource-id='be.bpost.nerocounterappnative.po:id/tv_great_job']")).isEnabled();
		
	if (MissingParcelsPresence1==true && MissingParcelsBut1==true)
        {
               // click on the confirm button
               WebElement MarkMissing = driver.findElementByXPath("//android.widget.CheckBox[@resource-id='be.bpost.nerocounterappnative.po:id/checkbox_missing']");
               MarkMissing.click();
               WebElement ConfirmMissing = driver.findElementByXPath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_next']");
               ConfirmMissing.click();
        }
		//AndroidElement SkipMissing = driver.findElementByXPath("//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_next']");
		//SkipMissing.click();
		
	}
	
	public void ValidErrMessRTS(String REBarcodeValue, String REErrMsg) throws Throwable {
		
		Thread.sleep(3000);
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		AndroidElement ManageYourShop = driver.findElementByXPath("//android.widget.TextView[@text='Manage your \n"
				+ "shop']");
		ManageYourShop.click();
		
		System.out.println("Inside Shop Management FLow");
		
		AndroidElement RTSFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Return to \n"
				+ " sender']");
		RTSFlowButton.click();
		System.out.println("RTS Flow");
		
		AndroidElement AllowRTS = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowRTS.click();
		
		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		Thread.sleep(3000);
		AndroidElement RTSBarManualEntry = driver.findElementByXPath("//android.widget.TextView[@text = 'Barcode no']");
		RTSBarManualEntry.click();
		System.out.println("Manual Barcode entry open");
		Thread.sleep(3000);
		AndroidElement RTSBarEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/et_barcode_no']");
		RTSBarEditBox.click();
		RTSBarEditBox.sendKeys(REBarcodeValue);
		Thread.sleep(3000);
		
		AndroidElement RTSSubmitBut = driver.findElementByXPath("//android.widget.ImageButton[@resource-id='be.bpost.nerocounterappnative.po:id/btn_submit']");
		RTSSubmitBut.click();
		
		AndroidElement ErrPopUp = driver.findElementByXPath("//android.widget.TextView[@text='"+ REErrMsg + "']");
		
		Thread.sleep(5000);
		if(ErrPopUp.isDisplayed())
		{
			FileUtils.copyFile(file, new File("Getting expected error message successfully.jpg"));
			System.out.println("Getting expected error message successfully");
		}
		else
		{
			FileUtils.copyFile(file, new File("Not Getting expected error message successfully after scan.jpg"));
			System.out.println("Not Getting expected error message successfully after scan");
		}
}
	public void RTSGreatJob() throws Throwable{
		Thread.sleep(3000);
		
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		boolean GreatJobPresence = driver.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']")).isDisplayed();
		
		if (GreatJobPresence==true)
        {
               
               WebElement GreatJobPresence1 = driver.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"));
               System.out.println("Great Job page received");
        }
		
		FileUtils.copyFile(file, new File("Pickup Great Job page.jpg"));
	}
}
