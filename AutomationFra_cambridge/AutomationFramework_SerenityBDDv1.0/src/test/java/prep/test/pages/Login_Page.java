package prep.test.pages;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Login_Page extends PageObject{
	
	public Login_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
	}
	
	public void verifyWelcomeScreenUI() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(1000);
		System.out.println("before click Start check-in of your parcel");
		AndroidElement StartCheckinBut = driver.findElementByXPath("//android.widget.Button[@text='Start check-in of your parcel']");
		String welcometext = StartCheckinBut.getText();
		String FindText = (welcometext.equalsIgnoreCase("Start check-in of your parcel"))?"Welcome page validated after launch":"Failed to get welcome page";
		
		FileUtils.copyFile(file, new File("PreparationWelcomeScreen.jpg"));
	}
	
	public void ChangeLang(String LangType) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(1000);
		System.out.println("before changing language");
		AndroidElement LangMenu = driver.findElementByXPath("//android.view.ViewGroup[@resource-id='be.bpost.preparationapp:id/menu_language']");
		LangMenu.click();
		AndroidElement LangChange = driver.findElementByXPath("//android.widget.TextView[@text='" + LangType + "']");
		LangChange.click();
		FileUtils.copyFile(file, new File("Language Change done.jpg"));
	}
	
	public void clickStartCheckIn() throws Throwable {	
		driver.hideKeyboard();
		Thread.sleep(1000);
		AndroidElement StartCheckinBut = driver.findElementByXPath("//android.widget.Button[@text='Start check-in of your parcel']");
		
		StartCheckinBut.click();
		System.out.println("After click Start check-in of your parcel");
		Thread.sleep(3000);	
	}
	
	public void NavigateToBarcodeScreen() throws Throwable{
		Thread.sleep(1000);
		AndroidElement AllowCamera = driver.findElementByXPath("//android.widget.Button[@text='While using the app']");
				
		if(AllowCamera.isDisplayed()==true)
		{
			System.out.println("Allow Camera pop-up appears and handled");
			AllowCamera.click();
			Thread.sleep(1000);	
			AndroidElement BarcodeNo = driver.findElementByXPath("//android.widget.TextView[@text='Barcode No']");
			BarcodeNo.click();
			System.out.println("Clicked on Barcode No");
		}
		else
		{
			System.out.println("Allow camera pop-up doesnot appear so no need to handle");
		}
		
		Thread.sleep(1000);		
	}

}
