package prep.test.pages;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_Shop_Stockmngt_Page extends PageObject {

	public Counter_Shop_Stockmngt_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void StockMngtOpen() throws Throwable {
		Thread.sleep(3000);
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		AndroidElement ManageYourShop = driver
				.findElementByXPath("//android.widget.TextView[@text='Manage your \n" + "shop']");
		ManageYourShop.click();

		System.out.println("Inside Shop Management FLow");

		AndroidElement IntakeFlowButton = driver.findElementByXPath("//android.widget.TextView[@text='Stock']");
		IntakeFlowButton.click();
		System.out.println("Stock Flow");
		Thread.sleep(1000);

		AndroidElement StockOrder = driver.findElementByXPath("//android.widget.Button[@text='Order']");
		String stockordertext = StockOrder.getText();
		String FindText = (stockordertext.equalsIgnoreCase("Order")) ? "List of stock orders"
				: "Failed to get list of stock orders";

		FileUtils.copyFile(file, new File("Stock Management flow.jpg"));
	}

	public void StockMngtPlaceOrder() throws Throwable {
		Thread.sleep(3000);
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		AndroidElement StockOrder = driver.findElementByXPath("//android.widget.Button[@text='Order']");
		StockOrder.click();

		AndroidElement PlaceStockOrder = driver.findElementByXPath("//android.widget.Button[@text='Place order']");
		PlaceStockOrder.click();

		FileUtils.copyFile(file, new File("Placed order on Stock Management flow.jpg"));
	}

	public void StockMngtReview() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(1000);

		AndroidElement ConfirmPlaceOrder = driver
				.findElementByXPath("//android.widget.CheckBox[@text ='I confirm to place an order for above items']");
		ConfirmPlaceOrder.click();

		System.out.println("Before Confirming Order");
		AndroidElement ConfirmStockMngtOrder = driver
				.findElementByXPath("//android.widget.Button[@text ='Confirm order']");
		ConfirmStockMngtOrder.click();
		System.out.println("After Confirming Order");
		FileUtils.copyFile(file, new File("Order confirmed.jpg"));
	}

	public void Orderoverview() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(1000);

		AndroidElement OrderButton = driver
				.findElementByXPath("//android.widget.TextView[@text ='Orders']");
		OrderButton.click();
		
		AndroidElement OrderDate = driver.findElementByXPath("//android.widget.TextView[@text='Order Date']");
		String OrderDatetext = OrderDate.getText();
		String FindText = (OrderDatetext.equalsIgnoreCase("Order Date")) ? "Order Date available"
				: "Failed to get order date field";
		
		AndroidElement OrderNo = driver.findElementByXPath("//android.widget.TextView[@text='Order number']");
		String OrderNotext = OrderNo.getText();
		String FindText1 = (OrderNotext.equalsIgnoreCase("Order number")) ? "Order Number available"
				: "Failed to get order no field";
		
		AndroidElement OrderStatus = driver.findElementByXPath("//android.widget.TextView[@text='Order status']");
		String OrderStatustext = OrderNo.getText();
		String FindText2 = (OrderNotext.equalsIgnoreCase("Order status")) ? "Order Status available"
				: "Failed to get order status field";
		
		
		System.out.println("After checking order overview");
		FileUtils.copyFile(file, new File("Order overview.jpg"));
	}
	public void StockMngtGreatJob() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		boolean GreatJobPresence = driver.findElement(By.xpath("//android.widget.TextView[@text ='Thank you for your order']"))
				.isDisplayed();

		if (GreatJobPresence == true) {

			WebElement GreatJobPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Thank you for your order']"));
			System.out.println("Thank you for your order received");
		}
		
	}

}
