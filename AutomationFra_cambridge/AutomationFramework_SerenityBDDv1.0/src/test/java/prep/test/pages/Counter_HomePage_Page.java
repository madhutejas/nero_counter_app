package prep.test.pages;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_HomePage_Page extends PageObject{
	
	public Counter_HomePage_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void verifyCounterHomePage() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		System.out.println("Inside Counter Home Page");
		Thread.sleep(1000);
		
		AndroidElement ServerNewCustomer = driver.findElementByXPath("//android.widget.TextView[@text='Serve new \n"
				+ "customer']");
		String counterwelcometext = ServerNewCustomer.getText();
		String FindText = (counterwelcometext.equalsIgnoreCase("Serve new customer"))?"Welcome page validated after launch":"Failed to get welcome page";
		
		FileUtils.copyFile(file, new File("CounterWelcomeScreen.jpg"));
			}

}