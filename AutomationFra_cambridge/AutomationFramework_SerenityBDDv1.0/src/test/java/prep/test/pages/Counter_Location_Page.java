package prep.test.pages;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Counter_Location_Page extends PageObject {
	
	public Counter_Location_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void verifyCounterLocation() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		
		AndroidElement ConfirmLocation = driver.findElementByXPath("//android.widget.TextView[@text='Confirm Location']");
				
		if(ConfirmLocation.isDisplayed())
		{
			System.out.println("Confirm location screen received");
			AndroidElement ConfirmLocationButton = driver.findElementByXPath("//android.widget.Button[@text='Confirm']");
			ConfirmLocationButton.click();
			
			Thread.sleep(2000);	
		}
		else
		{
			System.out.println("Select location screen received");
			AndroidElement SelectLocationButton = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.pp:id/edit_location_search']");
			SelectLocationButton.click();
			Thread.sleep(2000);	
			String location = "12243";
			Thread.sleep(2000);
			SelectLocationButton.sendKeys(""+location.charAt(0));
				
			Thread.sleep(2000);	
			System.out.println("Confirm location screen received");
			AndroidElement ConfirmLocationButton = driver.findElementByXPath("//android.widget.Button[@text='Confirm']");
			ConfirmLocationButton.click();
		}
		
		FileUtils.copyFile(file, new File("CounterPOApk_location.jpg"));
	}

}
