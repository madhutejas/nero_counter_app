package prep.test.pages;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_Location_Page1 extends PageObject{
	
	public Counter_Location_Page1(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void verifyCounterLocation1() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		System.out.println("Inside location selection");
		Thread.sleep(3000);
		AndroidElement LocationSearchTextbox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.po:id/edit_location_search']");
        LocationSearchTextbox.click();
        System.out.println("before entering pincode");
        Thread.sleep(1000);
        LocationSearchTextbox.sendKeys("12243");
        System.out.println("After entring pincode");
        driver.hideKeyboard();
       
        Thread.sleep(2000);
        AndroidElement selectLoc = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='be.bpost.nerocounterappnative.po:id/rl_main']");
        selectLoc.click();
        Thread.sleep(3000);
       
        //click confirm page
        System.out.println("before confirm");
        AndroidElement confirmLocation = driver.findElementByXPath("//android.widget.Button[@resource-id='be.bpost.nerocounterappnative.po:id/btn_confirm']");
        confirmLocation.click();
		
		FileUtils.copyFile(file, new File("CounterPOApk_location.jpg"));
	}
	
	public void verifyCounterLocation_PP() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(1000);
		System.out.println("Inside location selection");
		Thread.sleep(1000);
		AndroidElement LocationSearchTextbox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.nerocounterappnative.pp:id/edit_location_search']");
        LocationSearchTextbox.click();
        System.out.println("before entering pincode");
        Thread.sleep(1000);
        LocationSearchTextbox.sendKeys("11986");
        System.out.println("After entring pincode");
        driver.hideKeyboard();
       
       // Thread.sleep(1000);
        AndroidElement selectLoc = driver.findElementByXPath("//android.widget.RelativeLayout[@resource-id='be.bpost.nerocounterappnative.pp:id/rl_main']");
        selectLoc.click();
        Thread.sleep(3000);
       
        //click confirm page
        System.out.println("before confirm");
        AndroidElement confirmLocation = driver.findElementByXPath("//android.widget.Button[@text='Confirm']");
        confirmLocation.click();
		
		FileUtils.copyFile(file, new File("CounterPPApk_location.jpg"));
	}

}
