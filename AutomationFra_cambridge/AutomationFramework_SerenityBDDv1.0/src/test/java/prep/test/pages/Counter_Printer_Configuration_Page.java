package prep.test.pages;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_Printer_Configuration_Page extends PageObject{
	
	public Counter_Printer_Configuration_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void SelectPrinter() throws Throwable{
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
			
		AndroidElement hamburgericon = driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/img_hamburger']");
		hamburgericon.click();
		
		AndroidElement settingssel = driver.findElementByXPath("//android.widget.TextView[@resource-id='be.bpost.nerocounterappnative.po:id/tv_settings']");
		settingssel.click();
		
		AndroidElement Deviceconfig = driver.findElementByXPath("//android.widget.FrameLayout[@resource-id='be.bpost.nerocounterappnative.po:id/cv_printer_configuration']");
		Deviceconfig.click();
		
	}
	
	public void SelectLabelPrinter() throws Throwable{
		AndroidElement labelprintdopdown = driver.findElementByXPath("//android.widget.TextView[@resource-id='android:id/text1']");
		labelprintdopdown.click();
		
		AndroidElement sellabelprinter = driver.findElementByXPath("//android.widget.TextView[@text='Mock Label printer']");
		sellabelprinter.click();
		
	}
	
	public void SelectReceiptPrinter() throws Throwable{
		AndroidElement receiptprintdopdown = driver.findElementByXPath("//android.widget.TextView[@text='Label printer 1']");
		receiptprintdopdown.click();
		
		AndroidElement selreceiptprinter = driver.findElementByXPath("//android.widget.TextView[@text='Mock Label printer']");
		selreceiptprinter.click();
		
	}
	
	public void SelectPaymentTer() throws Throwable{
		AndroidElement paymentterdopdown = driver.findElementByXPath("//android.widget.TextView[@text='Yomani ML ID - 01330401']");
		paymentterdopdown.click();
		
		AndroidElement selpaymenttermprinter = driver.findElementByXPath("//android.widget.TextView[@text='Yomani ML ID - 00000000']");
		selpaymenttermprinter.click();
		
	}
	
	public void confirmprinter() throws Throwable{
		AndroidElement printerconfirmbut = driver.findElementByXPath("//android.widget.Button[@text='Confirm']");
		printerconfirmbut.click();
		
		AndroidElement backfrmprinterconfig = driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/btn_back']");
		backfrmprinterconfig.click();
		
	}
	}
