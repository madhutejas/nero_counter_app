package prep.test.pages;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class BarcodeEnter_Page extends PageObject {

	public BarcodeEnter_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
	}

	public void EnterBarcodeForScan(String BarcodeType,String BarcodeValue) throws Throwable{
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Barcode entering loaded successfully.jpg"));
		AndroidElement BarEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/et_barcode_num']");
		BarEditBox.click();
		BarEditBox.sendKeys(BarcodeValue);
		Thread.sleep(2000);
		AndroidElement SubmitBar = driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.preparationapp:id/iv_submit']");
		SubmitBar.click();
		AndroidElement ConfirmBut = driver.findElementByXPath("//android.widget.Button[@text='Confirm(1)']");
		if(ConfirmBut.isEnabled()==true)
		{
			ConfirmBut.click();
			if(BarcodeType.equalsIgnoreCase("Complete"))
			{
				Thread.sleep(2000);
				AndroidElement CheckInDone = driver.findElementByXPath("//android.widget.TextView[@text='Check-in completed']");
				if(CheckInDone.isDisplayed())
				{
					FileUtils.copyFile(file, new File("Barcode successfully sent for CheckIn.jpg"));
					System.out.println("Barcode contains complete details so sent for check-in");
					//Clicking Home after check-in
					driver.findElementByXPath("//android.widget.Button[@text='Home']").click();
				}
				else
				{
					System.out.println("Something went wrong after scanning barcode...Please check");
				}
			}
			else if(BarcodeType.equalsIgnoreCase("InComplete"))
			{
				Thread.sleep(2000);
				AndroidElement CheckInOK = driver.findElementByXPath("//android.widget.Button[@text='OK']");
				if(CheckInOK.isDisplayed())
				{
					FileUtils.copyFile(file, new File("Checkin Page Started.jpg"));
					System.out.println("Checkin Page Started by  clicking OK");
					//Clicking OK after scanning
					CheckInOK.click();
				}
			
			}
			else
			{
				//Write code here for error validation
				System.out.println("Getting error as expected for wrong barcode...Please check");
				FileUtils.copyFile(file, new File("Getting error as expected.jpg"));
			}
		}
		else
		{
			System.out.println("Barcode not get identified...Please check");
		}
	}
	
	public void ValidErrMess(String BarcodeValue, String ErrMsg) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		FileUtils.copyFile(file, new File("Barcode entering page loaded successfully.jpg"));
		AndroidElement BarEditBox = driver.findElementByXPath("//android.widget.EditText[@resource-id='be.bpost.preparationapp:id/et_barcode_num']");
		BarEditBox.click();
		BarEditBox.sendKeys(BarcodeValue);
		Thread.sleep(2000);
		AndroidElement SubmitBar = driver.findElementByXPath("//android.widget.ImageView[@resource-id='be.bpost.preparationapp:id/iv_submit']");
		SubmitBar.click();
		AndroidElement ErrPopUp = driver.findElementByXPath("//android.widget.TextView[@text='"+ ErrMsg + "']");
		Thread.sleep(5000);
		if(ErrPopUp.isDisplayed())
		{
			FileUtils.copyFile(file, new File("Getting expected error message successfully.jpg"));
			System.out.println("Getting expected error message successfully");
		}
		else
		{
			FileUtils.copyFile(file, new File("Not Getting expected error message successfully after scan.jpg"));
			System.out.println("Not Getting expected error message successfully after scan");
		}
	}
}
