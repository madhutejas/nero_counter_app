package prep.test.pages;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_Customer_SimpleFranking_Page extends PageObject {

	public Counter_Customer_SimpleFranking_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void SimpleFrankingtype(String SFrankingitem) throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");
		
		AndroidElement Allowcamera = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		Allowcamera.click();

		switch (SFrankingitem) {
		case "Prior Letter":// for Prior letter - Belgium
			System.out.println("Selecting Prior letter - Belgium");
			AndroidElement priorletter = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
			priorletter.click();

			AndroidElement printnadd = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnadd.click();

			AndroidElement closebasket = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasket.click();

			// Add Not Normalised - 0-100 weight

			AndroidElement priorletter1 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
			priorletter1.click();

			AndroidElement priornotnorm_0to100 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnorm_0to100.click();

			AndroidElement printnadd1 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnadd1.click();

			AndroidElement closebasket1 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasket1.click();

			// Add Not Normalised - 101-350 weight

			AndroidElement priorletter2 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
			priorletter2.click();

			AndroidElement priornotnorm = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnorm.click();

			AndroidElement priornotnorm_101to350 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
			priornotnorm_101to350.click();

			AndroidElement printnadd2 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnadd2.click();

			AndroidElement closebasket2 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasket2.click();

			// Add Not Normalised - 351-1kg weight

			AndroidElement priorletter3 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
			priorletter2.click();

			AndroidElement priornotnorm1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnorm.click();

			AndroidElement priornotnorm_351to1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
			priornotnorm_351to1.click();

			AndroidElement printnadd3 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnadd2.click();

			AndroidElement closebasket3 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasket3.click();

			// Add Not Normalised - 1kg-2kg weight

			AndroidElement priorletter4 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
			priorletter4.click();

			AndroidElement priornotnorm2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnorm2.click();

			AndroidElement priornotnorm_1to2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
			priornotnorm_1to2.click();

			AndroidElement printnadd4 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnadd4.click();

			break;
		case "Non Prior Letter":// for Non Prior letter Belgium
			System.out.println("Selecting Non Prior letter Belgium");
			AndroidElement nonpriorletter = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
			nonpriorletter.click();

			AndroidElement printnaddnonprior = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddnonprior.click();

			AndroidElement closebasketnonprior = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketnonprior.click();

			// Add Not Normalised - 0-100 weight

			AndroidElement nonpriorletter1 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
			nonpriorletter1.click();

			AndroidElement npriornotnorm_0to100 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			npriornotnorm_0to100.click();

			AndroidElement printnaddnonprior1 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddnonprior1.click();

			AndroidElement closebasketnonprior1 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketnonprior1.click();

			// Add Not Normalised - 101-350 weight

			AndroidElement nonpriorletter2 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
			nonpriorletter2.click();

			AndroidElement npriornotnorm = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			npriornotnorm.click();

			AndroidElement npriornotnorm_101to350 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
			npriornotnorm_101to350.click();

			AndroidElement printnaddnonprior2 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddnonprior2.click();

			AndroidElement closebasketnonprior2 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketnonprior2.click();

			// Add Not Normalised - 351-1kg weight

			AndroidElement nonpriorletter3 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
			nonpriorletter3.click();

			AndroidElement npriornotnorm1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			npriornotnorm1.click();

			AndroidElement npriornotnorm_351to1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
			npriornotnorm_351to1.click();

			AndroidElement printnaddnonprior3 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddnonprior3.click();

			AndroidElement closebasketnonprior3 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketnonprior3.click();

			// Add Not Normalised - 1kg-2kg weight

			AndroidElement nonpriorletter4 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
			nonpriorletter4.click();

			AndroidElement npriornotnorm2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			npriornotnorm2.click();

			AndroidElement npriornotnorm_1to2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
			npriornotnorm_1to2.click();

			AndroidElement printnaddnonprior4 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddnonprior4.click();

			break;
		case "Registered letter Belgium":// for Registered letter Belgium
			System.out.println("Selecting Registered letter Belgium");
			AndroidElement registeredletter = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter.click();

			AndroidElement printnaddregletter = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter.click();

			AndroidElement closebasketregletter = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter.click();

			// with confirmation of delivery of paper

			AndroidElement registeredlettercod = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredlettercod.click();

			AndroidElement selectcheckboxcod = driver.findElementByXPath(
					"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
			selectcheckboxcod.click();

			AndroidElement printnaddreglettercod = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddreglettercod.click();

			AndroidElement closebasketreglettercod = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketreglettercod.click();

			// Add Not Normalised - 0-100 weight

			AndroidElement registeredletter1 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter1.click();

			AndroidElement regletternotnorm_0to100 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnorm_0to100.click();

			AndroidElement printnaddregletter1 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter1.click();

			AndroidElement closebasketregletter1 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter1.click();

			// Add Not Normalised - 0-100 weight with confirmation of delivery of paper

			AndroidElement registeredletter1cod = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter1cod.click();

			AndroidElement regletternotnorm_0to100cod = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnorm_0to100cod.click();

			AndroidElement selectcheckboxcod1 = driver.findElementByXPath(
					"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
			selectcheckboxcod1.click();

			AndroidElement printnaddregletter1cod = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter1cod.click();

			AndroidElement closebasketregletter1cod = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter1cod.click();

			// Add Not Normalised - 101-350 weight

			AndroidElement registeredletter2 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter2.click();

			AndroidElement regletternotnorm = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnorm.click();

			AndroidElement regletternotnorm_101to350 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
			regletternotnorm_101to350.click();

			AndroidElement printnaddregletter2 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter2.click();

			AndroidElement closebasketregletter2 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter2.click();

			// Add Not Normalised - 101-350 weight with confirmation of delivery of paper

			AndroidElement registeredletter2cod = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter2cod.click();

			AndroidElement regletternotnormcod = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnormcod.click();

			AndroidElement regletternotnorm_101to350cod = driver
					.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
			regletternotnorm_101to350cod.click();

			AndroidElement selectcheckboxcod2 = driver.findElementByXPath(
					"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
			selectcheckboxcod2.click();

			AndroidElement printnaddregletter2cod = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter2cod.click();

			AndroidElement closebasketregletter2cod = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter2cod.click();

			// Add Not Normalised - 351-1kg weight

			AndroidElement registeredletter3 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter3.click();

			AndroidElement regletternotnorm1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnorm1.click();

			AndroidElement regletternotnorm_351to1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
			regletternotnorm_351to1.click();

			AndroidElement printnaddregletter3 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter3.click();

			AndroidElement closebasketregletter3 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter3.click();

			// Add Not Normalised - 351-1kg weight with confirmation of delivery of paper

			AndroidElement registeredletter3cod = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter3cod.click();

			AndroidElement regletternotnorm1cod = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnorm1cod.click();

			AndroidElement regletternotnorm_351to1cod = driver
					.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
			regletternotnorm_351to1cod.click();

			AndroidElement selectcheckboxcod3 = driver.findElementByXPath(
					"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
			selectcheckboxcod3.click();

			AndroidElement printnaddregletter3cod = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter3cod.click();

			AndroidElement closebasketregletter3cod = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter3cod.click();

			// Add Not Normalised - 1kg-2kg weight

			AndroidElement registeredletter4 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter4.click();

			AndroidElement regletternotnorm2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnorm2.click();

			AndroidElement regletternotnorm_1to2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
			regletternotnorm_1to2.click();

			AndroidElement printnaddregletter4 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter4.click();

			AndroidElement closebasketregletter4 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter4.click();

			// Add Not Normalised - 1kg-2kg weight with confirmation of delivery of paper

			AndroidElement registeredletter4cod = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter4cod.click();

			AndroidElement regletternotnorm2cod = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnorm2cod.click();

			AndroidElement regletternotnorm_1to2cod = driver
					.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
			regletternotnorm_1to2cod.click();

			AndroidElement selectcheckboxcod5 = driver.findElementByXPath(
					"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
			selectcheckboxcod5.click();

			AndroidElement printnaddregletter4cod = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter4cod.click();

			AndroidElement closebasketregletter5 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter5.click();

			// Add Not Normalised - 2kg-10kg weight

			AndroidElement registeredletter5 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter5.click();

			AndroidElement regletternotnorm3 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnorm3.click();

			AndroidElement regletternotnorm_2to10 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
			regletternotnorm_2to10.click();

			AndroidElement printnaddregletter5 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter5.click();

			AndroidElement closebasketregletter6 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketregletter6.click();

			// Add Not Normalised - 2kg-10kg weight with confirmation of delivery of paper

			AndroidElement registeredletter5cod = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
			registeredletter5cod.click();

			AndroidElement regletternotnorm3cod = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			regletternotnorm3cod.click();

			AndroidElement regletternotnorm_2to10cod = driver
					.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
			regletternotnorm_2to10cod.click();

			AndroidElement selectcheckboxcod6 = driver.findElementByXPath(
					"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
			selectcheckboxcod6.click();

			AndroidElement printnaddregletter5cod = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddregletter5cod.click();

			break;

		case "Parcel Belgium - Drop Yes":// for Parcel Belgium with Drop Yes option
			System.out.println("Selecting Parcel Belgium");
			AndroidElement parcelbelgium = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgium.click();

			AndroidElement printnaddparcel = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparcel.click();

			AndroidElement dropyes = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
			dropyes.click();

			AndroidElement closebasketparcel = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparcel.click();

			// with warranty

			AndroidElement parcelbelgium1 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgium1.click();

			AndroidElement addwarranty = driver
					.findElementByXPath("//android.widget.CheckBox[@text='Would you like a warranty up to €500?']");
			addwarranty.click();

			AndroidElement printnaddparcelwarranty = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparcelwarranty.click();

			AndroidElement dropyeswarranty = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
			dropyeswarranty.click();

			AndroidElement closebasketparcelwarranty = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparcelwarranty.click();

			// Parcel - 2kg to 10kg

			AndroidElement parcelbelgium2 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgium2.click();

			AndroidElement parcelbelgium2kgto10kg = driver
					.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
			parcelbelgium2kgto10kg.click();

			AndroidElement printnaddparcel2 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparcel2.click();

			AndroidElement dropyes2 = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
			dropyes2.click();

			AndroidElement closebasketparcel2 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparcel2.click();

			// 2kg to 10kg with warranty

			AndroidElement parcelbelgium3 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgium3.click();

			AndroidElement parcelbelgium2kgto10kgwa = driver
					.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
			parcelbelgium2kgto10kgwa.click();

			AndroidElement addwarranty1 = driver
					.findElementByXPath("//android.widget.CheckBox[@text='Would you like a warranty up to €500?']");
			addwarranty1.click();

			AndroidElement printnaddparcelwarranty1 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparcelwarranty1.click();

			AndroidElement dropyeswarranty1 = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
			dropyeswarranty1.click();

			AndroidElement closebasketparcelwarranty1 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparcelwarranty1.click();

			// Parcel - 10kg to 30kg

			AndroidElement parcelbelgium4 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgium4.click();

			AndroidElement parcelbelgium10kgto30kg = driver
					.findElementByXPath("//android.widget.RadioButton[@text='10Kg - 30Kg']");
			parcelbelgium10kgto30kg.click();

			AndroidElement printnaddparcel4 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparcel4.click();

			AndroidElement dropyes4 = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
			dropyes4.click();
			Thread.sleep(1000);

			/*AndroidElement closebasketparcel4 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparcel4.click();*/
			break;
			
		case "Parcel Belgium - Drop No":// for Parcel Belgium with Drop Yes option
			System.out.println("inside Selecting Parcel Belgium for drop No");
			AndroidElement parcelbelgiumdropno = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgiumdropno.click();

			AndroidElement printnaddparceldropno = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparceldropno.click();

			AndroidElement dropno = driver.findElementByXPath("//android.widget.Button[@text='No']");
			dropno.click();

			AndroidElement closebasketparceldropno = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparceldropno.click();

			// with warranty

			AndroidElement parcelbelgiumdropno1 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgiumdropno1.click();

			AndroidElement addwarrantydropno = driver
					.findElementByXPath("//android.widget.CheckBox[@text='Would you like a warranty up to €500?']");
			addwarrantydropno.click();

			AndroidElement printnaddparceldropnowa = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparceldropnowa.click();

			AndroidElement dropnowarranty = driver.findElementByXPath("//android.widget.Button[@text='No']");
			dropnowarranty.click();

			AndroidElement closebasketparcelwa = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparcelwa.click();

			// Parcel - 2kg to 10kg

			AndroidElement parcelbelgiumdropno2 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgiumdropno2.click();

			AndroidElement parcelbelgium2kgto10kgdn = driver
					.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
			parcelbelgium2kgto10kgdn.click();

			AndroidElement printnaddparceldropno1 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparceldropno1.click();

			AndroidElement dropno2 = driver.findElementByXPath("//android.widget.Button[@text='No']");
			dropno2.click();

			AndroidElement closebasketparceldn2 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparceldn2.click();

			// 2kg to 10kg with warranty

			AndroidElement parcelbelgiumdropno3 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgiumdropno3.click();

			AndroidElement parcelbelgium2kgto10kgdn1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
			parcelbelgium2kgto10kgdn1.click();

			AndroidElement addwarrantydropno1 = driver
					.findElementByXPath("//android.widget.CheckBox[@text='Would you like a warranty up to €500?']");
			addwarrantydropno1.click();

			AndroidElement printnaddparceldropno1wa = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparceldropno1wa.click();

			AndroidElement dropnowarranty1 = driver.findElementByXPath("//android.widget.Button[@text='No']");
			dropnowarranty1.click();

			AndroidElement closebasketparcelwa1 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparcelwa1.click();

			// Parcel - 10kg to 30kg

			AndroidElement parcelbelgiumdropno4 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
			parcelbelgiumdropno4.click();

			AndroidElement parcelbelgium10kgto30kgdn = driver
					.findElementByXPath("//android.widget.RadioButton[@text='10Kg - 30Kg']");
			parcelbelgium10kgto30kgdn.click();

			AndroidElement printnaddparceldropno2 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddparceldropno2.click();

			AndroidElement dropno4 = driver.findElementByXPath("//android.widget.Button[@text='No']");
			dropno4.click();

			/*AndroidElement closebasketparceldn4 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketparceldn4.click();
*/
			break;
			
		case "Prior letter Europe":// for Prior letter Europe
			System.out.println("Selecting Prior letter Europe");
			AndroidElement priorlettereurope = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
			priorlettereurope.click();

			AndroidElement printnaddeurope = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddeurope.click();

			AndroidElement closebasketeurope = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketeurope.click();

			// Add Not Normalised - 0-100 weight

			AndroidElement priorlettereurope1 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
			priorlettereurope1.click();

			AndroidElement priornotnormeu_0to100 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnormeu_0to100.click();

			AndroidElement printnaddeurope1 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddeurope1.click();

			AndroidElement closebasketeurope1 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketeurope1.click();

			// Add Not Normalised - 101-350 weight

			AndroidElement priorlettereurope2 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
			priorlettereurope2.click();

			AndroidElement priornotnormeu = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnormeu.click();

			AndroidElement priornotnormeu_101to350 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
			priornotnormeu_101to350.click();

			AndroidElement printnaddeurope2 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddeurope2.click();

			AndroidElement closebasketeurope2 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketeurope2.click();

			// Add Not Normalised - 351-1kg weight

			AndroidElement priorlettereurope3 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
			priorlettereurope3.click();

			AndroidElement priornotnormeu1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnormeu1.click();

			AndroidElement priornotnormeu_351to1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
			priornotnormeu_351to1.click();

			AndroidElement printnaddeurope3 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddeurope3.click();

			AndroidElement closebasketeurope3 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketeurope3.click();

			// Add Not Normalised - 1kg-2kg weight

			AndroidElement priorlettereurope4 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
			priorlettereurope4.click();

			AndroidElement priornotnormeu2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnormeu2.click();

			AndroidElement priornotnormeu_1to2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
			priornotnormeu_1to2.click();

			AndroidElement printnaddeurope4 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddeurope4.click();

			break;

		case "Prior letter Rest of the world":// for Prior letter Rest of the world
			System.out.println("Selecting Prior letter Rest of the world");
			AndroidElement priorlettereurow = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
			priorlettereurow.click();

			AndroidElement printnaddw = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddw.click();

			AndroidElement closebasketw = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketw.click();

			// Add Not Normalised - 0-100 weight

			AndroidElement priorlettereurow1 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
			priorlettereurow1.click();

			AndroidElement printnaddw_0to100 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			printnaddw_0to100.click();

			AndroidElement printnaddw1 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddw1.click();

			AndroidElement closebasketw1 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketw1.click();

			// Add Not Normalised - 101-350 weight

			AndroidElement priorlettereurow2 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
			priorlettereurow2.click();

			AndroidElement printnaddw2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			printnaddw2.click();

			AndroidElement priornotnormw_101to350 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
			priornotnormw_101to350.click();

			AndroidElement printnaddw3 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddw3.click();

			AndroidElement closebasketw2 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketw2.click();

			// Add Not Normalised - 351-1kg weight

			AndroidElement priorlettereurow3 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
			priorlettereurow3.click();

			AndroidElement priornotnormw1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnormw1.click();

			AndroidElement priornotnorw_351to1 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
			priornotnorw_351to1.click();

			AndroidElement printnaddw4 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddw4.click();

			AndroidElement closebasketw3 = driver.findElementByXPath(
					"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
			closebasketw3.click();

			// Add Not Normalised - 1kg-2kg weight

			AndroidElement priorlettereurow4 = driver
					.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
			priorlettereurow4.click();

			AndroidElement priornotnormw2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
			priornotnormw2.click();

			AndroidElement priornotnormw_1to2 = driver
					.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
			priornotnormw_1to2.click();

			AndroidElement printnaddw5 = driver
					.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
			printnaddw5.click();

			break;
		}
	}

	public void SimpleFrankingAll() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");
		
		AndroidElement AllowDrop = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		AllowDrop.click();
		// for Prior letter - Belgium
		System.out.println("Selecting Prior letter - Belgium");
		AndroidElement priorletter = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
		priorletter.click();

		AndroidElement printnadd = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnadd.click();

		AndroidElement closebasket = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasket.click();

		// Add Not Normalised - 0-100 weight

		AndroidElement priorletter1 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
		priorletter1.click();

		AndroidElement priornotnorm_0to100 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnorm_0to100.click();

		AndroidElement printnadd1 = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnadd1.click();

		AndroidElement closebasket1 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasket1.click();

		// Add Not Normalised - 101-350 weight

		AndroidElement priorletter2 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
		priorletter2.click();

		AndroidElement priornotnorm = driver.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnorm.click();

		AndroidElement priornotnorm_101to350 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
		priornotnorm_101to350.click();

		AndroidElement printnadd2 = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnadd2.click();

		AndroidElement closebasket2 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasket2.click();

		// Add Not Normalised - 351-1kg weight

		AndroidElement priorletter3 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
		priorletter2.click();

		AndroidElement priornotnorm1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnorm.click();

		AndroidElement priornotnorm_351to1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
		priornotnorm_351to1.click();

		AndroidElement printnadd3 = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnadd2.click();

		AndroidElement closebasket3 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasket3.click();

		// Add Not Normalised - 1kg-2kg weight

		AndroidElement priorletter4 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
		priorletter4.click();

		AndroidElement priornotnorm2 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnorm2.click();

		AndroidElement priornotnorm_1to2 = driver.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
		priornotnorm_1to2.click();

		AndroidElement printnadd4 = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnadd4.click();
		
		AndroidElement closebasketall = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketall.click();

		// for Non Prior letter Belgium
		System.out.println("Selecting Non Prior letter Belgium");
		AndroidElement nonpriorletter = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
		nonpriorletter.click();

		AndroidElement printnaddnonprior = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddnonprior.click();

		AndroidElement closebasketnonprior = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketnonprior.click();

		// Add Not Normalised - 0-100 weight

		AndroidElement nonpriorletter1 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
		nonpriorletter1.click();

		AndroidElement npriornotnorm_0to100 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		npriornotnorm_0to100.click();

		AndroidElement printnaddnonprior1 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddnonprior1.click();

		AndroidElement closebasketnonprior1 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketnonprior1.click();

		// Add Not Normalised - 101-350 weight

		AndroidElement nonpriorletter2 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
		nonpriorletter2.click();

		AndroidElement npriornotnorm = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		npriornotnorm.click();

		AndroidElement npriornotnorm_101to350 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
		npriornotnorm_101to350.click();

		AndroidElement printnaddnonprior2 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddnonprior2.click();

		AndroidElement closebasketnonprior2 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketnonprior2.click();

		// Add Not Normalised - 351-1kg weight

		AndroidElement nonpriorletter3 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
		nonpriorletter3.click();

		AndroidElement npriornotnorm1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		npriornotnorm1.click();

		AndroidElement npriornotnorm_351to1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
		npriornotnorm_351to1.click();

		AndroidElement printnaddnonprior3 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddnonprior3.click();

		AndroidElement closebasketnonprior3 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketnonprior3.click();

		// Add Not Normalised - 1kg-2kg weight

		AndroidElement nonpriorletter4 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Non Prior letter Belgium']");
		nonpriorletter4.click();

		AndroidElement npriornotnorm2 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		npriornotnorm2.click();

		AndroidElement npriornotnorm_1to2 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
		npriornotnorm_1to2.click();

		AndroidElement printnaddnonprior4 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddnonprior4.click();
		
		AndroidElement closebasketall1 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketall1.click();

		// for Registered letter Belgium
		System.out.println("Selecting Registered letter Belgium");
		AndroidElement registeredletter = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter.click();

		AndroidElement printnaddregletter = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter.click();

		AndroidElement closebasketregletter = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter.click();

		// with confirmation of delivery of paper

		AndroidElement registeredlettercod = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredlettercod.click();

		AndroidElement selectcheckboxcod = driver.findElementByXPath(
				"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
		selectcheckboxcod.click();

		AndroidElement printnaddreglettercod = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddreglettercod.click();

		AndroidElement closebasketreglettercod = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketreglettercod.click();

		// Add Not Normalised - 0-100 weight

		AndroidElement registeredletter1 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter1.click();

		AndroidElement regletternotnorm_0to100 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnorm_0to100.click();

		AndroidElement printnaddregletter1 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter1.click();

		AndroidElement closebasketregletter1 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter1.click();

		// Add Not Normalised - 0-100 weight with confirmation of delivery of paper

		AndroidElement registeredletter1cod = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter1cod.click();

		AndroidElement regletternotnorm_0to100cod = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnorm_0to100cod.click();

		AndroidElement selectcheckboxcod1 = driver.findElementByXPath(
				"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
		selectcheckboxcod1.click();

		AndroidElement printnaddregletter1cod = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter1cod.click();

		AndroidElement closebasketregletter1cod = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter1cod.click();

		// Add Not Normalised - 101-350 weight

		AndroidElement registeredletter2 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter2.click();

		AndroidElement regletternotnorm = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnorm.click();

		AndroidElement regletternotnorm_101to350 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
		regletternotnorm_101to350.click();

		AndroidElement printnaddregletter2 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter2.click();

		AndroidElement closebasketregletter2 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter2.click();

		// Add Not Normalised - 101-350 weight with confirmation of delivery of paper

		AndroidElement registeredletter2cod = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter2cod.click();

		AndroidElement regletternotnormcod = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnormcod.click();

		AndroidElement regletternotnorm_101to350cod = driver
				.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
		regletternotnorm_101to350cod.click();

		AndroidElement selectcheckboxcod2 = driver.findElementByXPath(
				"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
		selectcheckboxcod2.click();

		AndroidElement printnaddregletter2cod = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter2cod.click();

		AndroidElement closebasketregletter2cod = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter2cod.click();

		// Add Not Normalised - 351-1kg weight
		

		AndroidElement registeredletter3 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter3.click();

		AndroidElement regletternotnorm1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnorm1.click();

		AndroidElement regletternotnorm_351to1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
		regletternotnorm_351to1.click();

		AndroidElement printnaddregletter3 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter3.click();

		AndroidElement closebasketregletter3 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter3.click();

		// Add Not Normalised - 351-1kg weight with confirmation of delivery of paper

		AndroidElement registeredletter3cod = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter3cod.click();

		AndroidElement regletternotnorm1cod = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnorm1cod.click();

		AndroidElement regletternotnorm_351to1cod = driver
				.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
		regletternotnorm_351to1cod.click();

		AndroidElement selectcheckboxcod3 = driver.findElementByXPath(
				"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
		selectcheckboxcod3.click();

		AndroidElement printnaddregletter3cod = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter3cod.click();

		AndroidElement closebasketregletter3cod = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter3cod.click();

		// Add Not Normalised - 1kg-2kg weight

		AndroidElement registeredletter4 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter4.click();

		AndroidElement regletternotnorm2 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnorm2.click();

		AndroidElement regletternotnorm_1to2 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
		regletternotnorm_1to2.click();

		AndroidElement printnaddregletter4 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter4.click();

		AndroidElement closebasketregletter4 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter4.click();

		// Add Not Normalised - 1kg-2kg weight with confirmation of delivery of paper

		AndroidElement registeredletter4cod = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter4cod.click();

		AndroidElement regletternotnorm2cod = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnorm2cod.click();

		AndroidElement regletternotnorm_1to2cod = driver
				.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
		regletternotnorm_1to2cod.click();

		AndroidElement selectcheckboxcod5 = driver.findElementByXPath(
				"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
		selectcheckboxcod5.click();

		AndroidElement printnaddregletter4cod = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter4cod.click();

		AndroidElement closebasketregletter5 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter5.click();

		// Add Not Normalised - 2kg-10kg weight

		AndroidElement registeredletter5 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter5.click();

		AndroidElement regletternotnorm3 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnorm3.click();

		AndroidElement regletternotnorm_2to10 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
		regletternotnorm_2to10.click();

		AndroidElement printnaddregletter5 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter5.click();

		AndroidElement closebasketregletter6 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketregletter6.click();

		// Add Not Normalised - 2kg-10kg weight with confirmation of delivery of paper

		AndroidElement registeredletter5cod = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Registered letter Belgium']");
		registeredletter5cod.click();

		AndroidElement regletternotnorm3cod = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		regletternotnorm3cod.click();

		AndroidElement regletternotnorm_2to10cod = driver
				.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
		regletternotnorm_2to10cod.click();

		AndroidElement selectcheckboxcod6 = driver.findElementByXPath(
				"//android.widget.CheckBox[@text='Would you like to receive a confirmation of delivery on paper?']");
		selectcheckboxcod6.click();

		AndroidElement printnaddregletter5cod = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddregletter5cod.click();
		
		AndroidElement closebasketall2 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketall2.click();

		// for Parcel Belgium with Drop Yes option
		System.out.println("Selecting Parcel Belgium");
		AndroidElement parcelbelgium = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
		parcelbelgium.click();

		AndroidElement printnaddparcel = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddparcel.click();

		AndroidElement dropyes = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
		dropyes.click();

		AndroidElement closebasketparcel = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketparcel.click();

		// with warranty

		AndroidElement parcelbelgium1 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
		parcelbelgium1.click();

		AndroidElement addwarranty = driver
				.findElementByXPath("//android.widget.CheckBox[@text='Would you like a warranty up to €500?']");
		addwarranty.click();

		AndroidElement printnaddparcelwarranty = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddparcelwarranty.click();

		AndroidElement dropyeswarranty = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
		dropyeswarranty.click();

		AndroidElement closebasketparcelwarranty = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketparcelwarranty.click();

		// Parcel - 2kg to 10kg

		AndroidElement parcelbelgium2 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
		parcelbelgium2.click();

		AndroidElement parcelbelgium2kgto10kg = driver
				.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
		parcelbelgium2kgto10kg.click();

		AndroidElement printnaddparcel2 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddparcel2.click();

		AndroidElement dropyes2 = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
		dropyes2.click();

		AndroidElement closebasketparcel2 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketparcel2.click();

		// 2kg to 10kg with warranty

		AndroidElement parcelbelgium3 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
		parcelbelgium3.click();

		AndroidElement parcelbelgium2kgto10kgwa = driver
				.findElementByXPath("//android.widget.RadioButton[@text='2Kg - 10Kg']");
		parcelbelgium2kgto10kgwa.click();

		AndroidElement addwarranty1 = driver
				.findElementByXPath("//android.widget.CheckBox[@text='Would you like a warranty up to €500?']");
		addwarranty1.click();

		AndroidElement printnaddparcelwarranty1 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddparcelwarranty1.click();

		AndroidElement dropyeswarranty1 = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
		dropyeswarranty1.click();

		AndroidElement closebasketparcelwarranty1 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketparcelwarranty1.click();

		// Parcel - 10kg to 30kg

		AndroidElement parcelbelgium4 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Parcel Belgium']");
		parcelbelgium4.click();

		AndroidElement parcelbelgium10kgto30kg = driver
				.findElementByXPath("//android.widget.RadioButton[@text='10Kg - 30Kg']");
		parcelbelgium10kgto30kg.click();

		AndroidElement printnaddparcel4 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddparcel4.click();

		AndroidElement dropyes4 = driver.findElementByXPath("//android.widget.Button[@text='Yes']");
		dropyes4.click();

		AndroidElement closebasketparcel4 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketparcel4.click();

		// for Prior letter Europe
		System.out.println("Selecting Prior letter Europe");
		AndroidElement priorlettereurope = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
		priorlettereurope.click();

		AndroidElement printnaddeurope = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddeurope.click();

		AndroidElement closebasketeurope = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketeurope.click();

		// Add Not Normalised - 0-100 weight

		AndroidElement priorlettereurope1 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
		priorlettereurope1.click();

		AndroidElement priornotnormeu_0to100 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnormeu_0to100.click();

		AndroidElement printnaddeurope1 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddeurope1.click();

		AndroidElement closebasketeurope1 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketeurope1.click();

		// Add Not Normalised - 101-350 weight

		AndroidElement priorlettereurope2 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
		priorlettereurope2.click();

		AndroidElement priornotnormeu = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnormeu.click();

		AndroidElement priornotnormeu_101to350 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
		priornotnormeu_101to350.click();

		AndroidElement printnaddeurope2 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddeurope2.click();

		AndroidElement closebasketeurope2 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketeurope2.click();

		// Add Not Normalised - 351-1kg weight

		AndroidElement priorlettereurope3 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
		priorlettereurope3.click();

		AndroidElement priornotnormeu1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnormeu1.click();

		AndroidElement priornotnormeu_351to1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
		priornotnormeu_351to1.click();

		AndroidElement printnaddeurope3 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddeurope3.click();

		AndroidElement closebasketeurope3 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketeurope3.click();

		// Add Not Normalised - 1kg-2kg weight

		AndroidElement priorlettereurope4 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Europe']");
		priorlettereurope4.click();

		AndroidElement priornotnormeu2 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnormeu2.click();

		AndroidElement priornotnormeu_1to2 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
		priornotnormeu_1to2.click();

		AndroidElement printnaddeurope4 = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddeurope4.click();
		
		AndroidElement closebasketall3 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketall3.click();

		// for Prior letter Rest of the world
		System.out.println("Selecting Prior letter Rest of the world");
		AndroidElement priorlettereurow = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
		priorlettereurow.click();

		AndroidElement printnaddw = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddw.click();

		AndroidElement closebasketw = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketw.click();

		// Add Not Normalised - 0-100 weight

		AndroidElement priorlettereurow1 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
		priorlettereurow1.click();

		AndroidElement printnaddw_0to100 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		printnaddw_0to100.click();

		AndroidElement printnaddw1 = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddw1.click();

		AndroidElement closebasketw1 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketw1.click();

		// Add Not Normalised - 101-350 weight

		AndroidElement priorlettereurow2 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
		priorlettereurow2.click();

		AndroidElement printnaddw2 = driver.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		printnaddw2.click();

		AndroidElement priornotnormw_101to350 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='101g - 350g']");
		priornotnormw_101to350.click();

		AndroidElement printnaddw3 = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddw3.click();

		AndroidElement closebasketw2 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketw2.click();

		// Add Not Normalised - 351-1kg weight

		AndroidElement priorlettereurow3 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
		priorlettereurow3.click();

		AndroidElement priornotnormw1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnormw1.click();

		AndroidElement priornotnorw_351to1 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='351g - 1Kg']");
		priornotnorw_351to1.click();

		AndroidElement printnaddw4 = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddw4.click();

		AndroidElement closebasketw3 = driver.findElementByXPath(
				"//android.widget.ImageView[@resource-id='be.bpost.nerocounterappnative.po:id/close_popup']");
		closebasketw3.click();

		// Add Not Normalised - 1kg-2kg weight

		AndroidElement priorlettereurow4 = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Rest of the world']");
		priorlettereurow4.click();

		AndroidElement priornotnormw2 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='Not Normalised']");
		priornotnormw2.click();

		AndroidElement priornotnormw_1to2 = driver
				.findElementByXPath("//android.widget.RadioButton[@text='1Kg - 2Kg']");
		priornotnormw_1to2.click();

		AndroidElement printnaddw5 = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnaddw5.click();

	}
	
	public void SimpleFrankingwithoutlogin() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		// for Prior letter - Belgium
		System.out.println("Selecting Prior letter - Belgium");
		AndroidElement priorletter = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
		priorletter.click();

		AndroidElement printnadd = driver.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnadd.click();
		
	}
		

	public void viewbasket() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		System.out.println("Inside view basket");

		AndroidElement viewbasket = driver.findElementByXPath("//android.widget.Button[@text='View basket']");
		viewbasket.click();
	}


	public void FrankingpaymentMaestro() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		
		System.out.println("In Shopping basket overview");
		Thread.sleep(10000);
		AndroidElement FinalizeShoppingBasketbutton = driver
				.findElementByXPath("//android.widget.Button[@text ='Finalize shopping basket']");
		FinalizeShoppingBasketbutton.click();
		System.out.println("Shopping basket finalized");
		
		Thread.sleep(1000);
		
		System.out.println("On payment screen");
		AndroidElement paymentMaestroconfirm = driver.findElementByXPath("//android.widget.Button[@bounds ='[16,688][1264,736]']");
		paymentMaestroconfirm.click();

		Thread.sleep(3000);

		System.out.println("Payment done");

		FileUtils.copyFile(file, new File("Payment successful with Maestro.jpg"));
	}
	
	public void FrankingpaymentVisa() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(90000);
		AndroidElement FinalizeShoppingBasketbutton = driver
				.findElementByXPath("//android.widget.Button[@text ='Finalize shopping basket']");
		FinalizeShoppingBasketbutton.click();

		Thread.sleep(9000);

		AndroidElement paymentvisamaster = driver.findElementByXPath("//android.widget.RadioButton[@text ='Visa/Mastercard   ']");
		paymentvisamaster.click();
		
		AndroidElement paymentvisaconfirm = driver.findElementByXPath("//android.widget.Button[@text ='Continue']");
		paymentvisaconfirm.click();

		Thread.sleep(3000);

		System.out.println("Payment done");

		FileUtils.copyFile(file, new File("Payment successful with Maestro.jpg"));
	}
	
	public void FrankingCustomerReceiptPrinting() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		boolean CustomerReceiptPresence = driver
				.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']")).isDisplayed();

		if (CustomerReceiptPresence == true) {

			WebElement CustomerReceiptPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']"));
			System.out.println("Customer Receipt page received");
			AndroidElement PrintReceiptLang = driver.findElementByXPath(
					"//android.widget.RadioButton[@resource-id ='be.bpost.nerocounterappnative.po:id/rb_en_print_receipt']");
			PrintReceiptLang.click();
			AndroidElement SubmitCustReceipt = driver.findElementByXPath("//android.widget.Button[@text ='Submit']");
			SubmitCustReceipt.click();
		}

		FileUtils.copyFile(file, new File("Customer Receipt page.jpg"));
	}
	
	public void FrankingCustomerReceiptDigitalreceipt() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		boolean CustomerReceiptPresence = driver
				.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']")).isDisplayed();

		if (CustomerReceiptPresence == true) {

			WebElement CustomerReceiptPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Customer’s receipt']"));
			System.out.println("Customer Receipt page received");
			AndroidElement DigitReceiptLang = driver.findElementByXPath("//android.widget.RadioButton[@text ='EN']");
			DigitReceiptLang.click();
			AndroidElement SubmitCustReceipt = driver.findElementByXPath("//android.widget.Button[@text ='Submit']");
			SubmitCustReceipt.click();
		}
		FileUtils.copyFile(file, new File("Customer Receipt page.jpg"));
	}

	public void FrankingGreatJob() throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		boolean GreatJobPresence = driver.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"))
				.isDisplayed();

		if (GreatJobPresence == true) {

			WebElement GreatJobPresence1 = driver
					.findElement(By.xpath("//android.widget.TextView[@text ='Great Job!']"));
			System.out.println("Great Job page received");
		}

		FileUtils.copyFile(file, new File("Drop Great Job page.jpg"));
	}
	
	public void ValidErrMessfranking(String FErrMsg) throws Throwable {

		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);

		AndroidElement ServerNewCustomer = driver
				.findElementByXPath("//android.widget.TextView[@text='Serve new \n" + "customer']");
		ServerNewCustomer.click();

		System.out.println("Inside Customer flow");
		
		AndroidElement Allowcamera = driver.findElementByXPath("//android.widget.Button[@text='Only this time']");
		Allowcamera.click();
		
		System.out.println("Selecting Prior letter - Belgium");
		AndroidElement priorletter = driver
				.findElementByXPath("//android.view.ViewGroup[@content-desc='Prior letter Belgium']");
		priorletter.click();

		AndroidElement printnadd = driver
				.findElementByXPath("//android.widget.Button[@text='Print & add to cart']");
		printnadd.click();

		AndroidElement ErrPopUp = driver.findElementByXPath("//android.widget.TextView[@text='Printer is not configured. \n" + 
				"Please go to Settings -> Device configuration and select the correct printer.']");
		Thread.sleep(5000);
		if (ErrPopUp.isDisplayed()) {
			FileUtils.copyFile(file, new File("Getting expected error message successfully.jpg"));
			System.out.println("Getting expected error message successfully");
		} else {
			FileUtils.copyFile(file, new File("Not Getting expected error message successfully after scan.jpg"));
			System.out.println("Not Getting expected error message successfully after scan");
		}
	}

}
