package prep.test.pages;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class ReviewDetails_Page extends PageObject{

	public ReviewDetails_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
	}
	
	public void ClickConfirmOREditDetails(String ClickType) throws Throwable {
		Thread.sleep(3000);
		if(ClickType.equalsIgnoreCase("Confirm")) {
			driver.findElementByXPath("//android.widget.Button[@text='Confirm']").click();
		}
		else
		{
			driver.findElementByXPath("//android.widget.Button[@text='Edit details']").click();
		}
	}
	
	public void VerifyCheckIn() throws Throwable {
		Thread.sleep(3000);
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		AndroidElement CheckInCompl = driver.findElementByXPath("//android.widget.TextView[@text='Check-in completed']");
		if(CheckInCompl.isDisplayed()) {
			System.out.println("CheckIn got completed and validated successfully");
			FileUtils.copyFile(file, new File("CheckIn got completed and validated successfully"));
			driver.findElementByXPath("//android.widget.Button[@text='Home']").click();
		}
		else
		{
			System.out.println("CheckIn got failed....Please check");
			FileUtils.copyFile(file, new File("CheckIn got failed....Please check"));
		}
	}

}
