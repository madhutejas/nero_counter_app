package prep.test.pages;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.PageObject;

public class Counter_Login_Page extends PageObject{
	
	public Counter_Login_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void verifyCounterLogin() throws Throwable {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(1000);
		
		AndroidElement Counter_Username = driver.findElementByXPath("//android.widget.EditText[@resource-id='username']");
		Counter_Username.sendKeys("u541142");
		
		AndroidElement Counter_Pass = driver.findElementByXPath("//android.widget.EditText[@resource-id='password']");
		Counter_Pass.sendKeys("Pran@123");
		
		Thread.sleep(1000);
		
		//AndroidElement SignOn = driver.findElementByXPath("//android.widget.TextView[@text='Sign On']");
		//SignOn.click();
		
		Thread.sleep(2000);
		System.out.println("App logged in");
		
		System.out.println("before click sign on");
	    driver.hideKeyboard();
	    Thread.sleep(1000);
	    //AndroidElement SignOnBtn = driver.findElement(By.xpath("//android.view.View[@text='Sign On']"));
	    AndroidElement SignOnBtn = driver.findElementByXPath("//android.view.View[@resource-id='signOnButton']");
	    SignOnBtn.click();
	    Thread.sleep(2000);
	    System.out.println("after click sign on");Thread.sleep(2000);
		
		FileUtils.copyFile(file, new File("CounterPOApk_Login.jpg"));
	}

}
