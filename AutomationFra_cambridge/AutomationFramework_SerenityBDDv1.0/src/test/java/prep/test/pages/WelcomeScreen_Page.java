package prep.test.pages;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class WelcomeScreen_Page extends PageObject{

	public WelcomeScreen_Page(AndroidDriver<AndroidElement> driver) {
		super(driver);
	}
	
	public void ValidateAppVer(String ExpAppVer) throws Exception {
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Thread.sleep(3000);
		System.out.println("get version num");
		ExpAppVer = "QA | App version: " + ExpAppVer;
		AndroidElement AppVer = driver.findElementByXPath("//android.widget.TextView[@text='" + ExpAppVer + "']");
		if(AppVer.getAttribute("text").equalsIgnoreCase(ExpAppVer)) {
			FileUtils.copyFile(file, new File("Validated appversion is getting as expected.jpg"));
			System.out.println("App version is getting as expected");
		}
		else
		{
			FileUtils.copyFile(file, new File("Failed to Validate appversion as value is not getting as expected.jpg"));
			System.out.println("App version is not getting as expected");
		}
		
	}
	
	
}
