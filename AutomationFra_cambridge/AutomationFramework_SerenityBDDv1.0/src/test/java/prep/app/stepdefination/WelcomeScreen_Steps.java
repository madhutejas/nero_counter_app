package prep.app.stepdefination;

import io.cucumber.java.en.*;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import prep.test.pages.WelcomeScreen_Page;

public class WelcomeScreen_Steps {

	AndroidDriver<AndroidElement> driver;
	WelcomeScreen_Page WelcomeScreen = new WelcomeScreen_Page(driver);
	
	@Then("Validate App Version {string}")
	public void validate_App_Version(String ExpAppVer) throws Exception {
		WelcomeScreen.ValidateAppVer(ExpAppVer);
	}
}
