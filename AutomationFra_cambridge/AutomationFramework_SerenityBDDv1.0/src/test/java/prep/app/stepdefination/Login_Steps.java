package prep.app.stepdefination;


import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import prep.test.pages.Login_Page;


public class Login_Steps{
	
	AndroidDriver<AndroidElement> driver;
	Login_Page loginPage = new Login_Page(driver);
	
	@Then("Verify UI of WelcomeScreen")
	public void verify_UI_of_WelcomeScreen() throws Throwable {
		loginPage.verifyWelcomeScreenUI();
	}
	
	@Then("Change the language of prep app to {string}")
	public void Change_Language_In_Prep_App(String LangType) throws Throwable {
		loginPage.ChangeLang(LangType);
	}

	@Then("Click StartCheckIn")
	public void Click_StartCheckIn() throws Throwable {
		loginPage.clickStartCheckIn();
	}
	
	@Then("Navigate To Barcode Screen")
	public void Navigate_To_Barcode_Screen() throws Throwable{
		loginPage.NavigateToBarcodeScreen();
	}	

}
