package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import prep.test.pages.Counter_Customer_Drop_Page;
import prep.test.pages.Counter_Customer_Pickup_Page;
import prep.test.pages.Counter_HomePage_Page;
import prep.test.pages.Counter_Location_Page;
import prep.test.pages.Counter_Location_Page1;

public class Counter_Customer_Pickup_Steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_Customer_Pickup_Page CounterCustPickup = new Counter_Customer_Pickup_Page(driver);

	@Then("^Enter the barcode for Pickup with value \"(.*)\"$")
	public void PickupBarcodeScan(String PBarcodeValue) throws Throwable {
		CounterCustPickup.PickupBarcodeScan(PBarcodeValue);
		CounterCustPickup.PickupBarcodeSubmit(PBarcodeValue);
		CounterCustPickup.PickupCustomerReceipt();
		CounterCustPickup.PickupBarcodeGreatJob(PBarcodeValue);
	}
	
	@Then("^Enter the barcode without checkout for Pickup with value \"(.*)\"$")
	public void PickupBarcodeScanwithout(String PBarcodeValue) throws Throwable {
		CounterCustPickup.PickupBarcodeScanwithaddmore(PBarcodeValue);
	}
	
	@Then("^Enter the Deliveredbarcode for Pickup with value \"(.*)\"$")
	public void PickupBarcodeAlreadyDelivered(String PBarcodeValue) throws Throwable {
		CounterCustPickup.PickupBarcodeAlreadyDelivered(PBarcodeValue);
		CounterCustPickup.PickupBarcodeSubmit(PBarcodeValue);
		CounterCustPickup.PickupCustomerReceipt();
		CounterCustPickup.PickupBarcodeGreatJob(PBarcodeValue);
	}
	
	@Then("^Enter the barcode for PickupSuggestion with value \"(.*)\"$")
	public void PickupBarcodeAddMoreItem(String PBarcodeValue) throws Throwable {
		CounterCustPickup.PickupBarcodeAddMoreItem(PBarcodeValue);
		CounterCustPickup.PickupBarcodeSubmit(PBarcodeValue);
		CounterCustPickup.PickupCustomerReceipt();
		CounterCustPickup.PickupBarcodeGreatJob(PBarcodeValue);
	}
	
	@When("^Validate wrong barcode in Pickup \"(.*)\" scan with its error message \"(.*)\"$")
	public void ValidErrMessPickup(String PEBarcodeValue, String PEErrMsg) throws Throwable {
		CounterCustPickup.ValidErrMessPickup(PEBarcodeValue, PEErrMsg);
	}
}
