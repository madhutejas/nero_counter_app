package prep.app.stepdefination;

import java.io.IOException;
import io.cucumber.java.en.Given;

public class Hook_Steps {

	@Given("Launching the Application in {string}")
	public void launchingTheApplicationIn(String device) throws IOException, Throwable {
	   Hook h = new Hook();
	   h.capabilitiesRealDevice(device);
	}
}
