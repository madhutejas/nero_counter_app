package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import prep.test.pages.Counter_Customer_Drop_Page;
import prep.test.pages.Counter_Customer_Intake_Page;
import prep.test.pages.Counter_Customer_Pickup_Page;
import prep.test.pages.Counter_HomePage_Page;
import prep.test.pages.Counter_Location_Page;
import prep.test.pages.Counter_Location_Page1;

public class Counter_Customer_Intake_Steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_Customer_Intake_Page CounterCustIntake = new Counter_Customer_Intake_Page(driver);

	@Then("Select the Intakeparcel type {string}")
	public void IntakeBarcodeType(String IBarcodeValue) throws Throwable {
		CounterCustIntake.IntakeBarcodeType(IBarcodeValue);
	}
	
	@Then("^Enter the barcode for Intake with value \"(.*)\"$")
	public void IntakeBarcodeValue(String IBarcodeValue) throws Throwable {
		CounterCustIntake.IntakeBarcodeValue(IBarcodeValue);
		CounterCustIntake.IntakeBarcodeValueSubmit();
		CounterCustIntake.ValidateIntakeGreatjob();
	}
	
	@Then("^Enter the barcode for ReIntake with value \"(.*)\"$")
	public void ReIntakeBarcodeValue(String RIBarcodeValue) throws Throwable {
		CounterCustIntake.ReIntakeBarcodeValue(RIBarcodeValue);
		CounterCustIntake.IntakeBarcodeValueSubmit();
		CounterCustIntake.ValidateIntakeGreatjob();
	}
	
	@When("^Validate wrong barcode in Intake \"(.*)\" scan with its error message \"(.*)\"$")
	public void ValidErrMessIntake(String IEBarcodeValue, String IEErrMsg) throws Throwable {
		CounterCustIntake.ValidErrMessIntake(IEBarcodeValue, IEErrMsg);
	}
}
