package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import prep.test.pages.Counter_Customer_Drop_Page;
import prep.test.pages.Counter_Customer_Pickup_Page;
import prep.test.pages.Counter_Customer_SimpleFranking_Page;
import prep.test.pages.Counter_HomePage_Page;
import prep.test.pages.Counter_Location_Page;
import prep.test.pages.Counter_Location_Page1;

public class Counter_Customer_SimpleFranking_Steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_Customer_SimpleFranking_Page CounterCustSimpleFranking = new Counter_Customer_SimpleFranking_Page(driver);

	@Then("Select franking items {string}")
	public void simpleFranking(String SFrankingitem) throws Throwable {
		CounterCustSimpleFranking.SimpleFrankingtype(SFrankingitem);
		CounterCustSimpleFranking.viewbasket();
		//CounterCustSimpleFranking.FrankingpaymentMaestro();
		//CounterCustSimpleFranking.FrankingCustomerReceiptPrinting();
		//CounterCustSimpleFranking.FrankingGreatJob();
	}
	
	@Then("Select franking items all")
	public void simpleFrankingall() throws Throwable {
		CounterCustSimpleFranking.SimpleFrankingAll();
		CounterCustSimpleFranking.viewbasket();
		CounterCustSimpleFranking.FrankingpaymentMaestro();
		CounterCustSimpleFranking.FrankingCustomerReceiptPrinting();
		CounterCustSimpleFranking.FrankingGreatJob();
	}
	
	@Then("Select franking item without login")
	public void simpleFrankingwithoutlogin() throws Throwable {
		CounterCustSimpleFranking.SimpleFrankingwithoutlogin();
		CounterCustSimpleFranking.viewbasket();
		CounterCustSimpleFranking.FrankingpaymentMaestro();
		CounterCustSimpleFranking.FrankingCustomerReceiptDigitalreceipt();
		CounterCustSimpleFranking.FrankingGreatJob();
	}
	
	@Then("Select franking item without login payment with Visa")
	public void simpleFrankingwithoutloginvisa() throws Throwable {
		CounterCustSimpleFranking.SimpleFrankingwithoutlogin();
		CounterCustSimpleFranking.viewbasket();
		CounterCustSimpleFranking.FrankingpaymentVisa();
		CounterCustSimpleFranking.FrankingCustomerReceiptDigitalreceipt();
		CounterCustSimpleFranking.FrankingGreatJob();
	}
	
	@When("^Validate wrong barcode in Franking scan with its error message \"(.*)\"$")
	public void ValidErrMessfrankingPrinterConfig(String FErrMsg) throws Throwable {
		CounterCustSimpleFranking.ValidErrMessfranking(FErrMsg);
	}
	
	}
