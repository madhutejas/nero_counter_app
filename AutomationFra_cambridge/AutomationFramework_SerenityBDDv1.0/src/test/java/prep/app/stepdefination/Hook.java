package prep.app.stepdefination;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;


public class Hook {

	public static AndroidDriver<AndroidElement> driver;
	public File f = new File("C:\\Users\\999282\\Desktop\\NeRO\\Automation\\madhutejas-nero_counter_app-669490d08963\\AutomationFramework_SerenityBDDv1.0_cambridge\\AutomationFramework_SerenityBDDv1.0\\src\\test\\resources\\Counter_apk"); 
	public File fs = new File(f, "PpQa-v2.8.9-PP(945).apk");
	
	public static AndroidDriver<AndroidElement> getDriver() {
		return driver;
	}

//	@Given("^Launching the Application in \"(.*)\"$")
	public void capabilitiesRealDevice(String device) throws IOException, Throwable {
		String usedDevice=device;
        switch(usedDevice) {
        case "1":
        	DesiredCapabilities capabilities = new DesiredCapabilities();
    		// Virtual device
    		capabilities.setCapability("deviceName", "10.1_WXGA_Tablet_API_30");
    		capabilities.setCapability("platformName", "Android");
    		//capabilities.setCapability("appPackage", "be.bpost.preparationapp");
    		//capabilities.setCapability("appActivity", "be.bpost.preparationapp.splash.presentation.SplashActivity");
    		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
    		capabilities.setCapability(MobileCapabilityType.APP,fs.getAbsolutePath()); 
    		Thread.sleep(2000);
    		System.out.println("Phone connected");
    		driver = new AndroidDriver<AndroidElement>(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
    		System.out.println("App Launched");
    		Thread.sleep(1000);
    		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    		FileUtils.copyFile(file, new File("Prep App Launched Successfully.jpg"));

    		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);   		    		
    		break;
        case "2":
    		DesiredCapabilities capabilities1 = new DesiredCapabilities();
    	    //for Emulator
    		Thread.sleep(2000);
    		//File f = new File("C:\\bpaid-appiumscripts-android\\bpaid\\Nero_Apks"); 
    		//File fs = new File(f, "nero-ctr-app-android-1.21.0.apk");
    		capabilities1.setCapability(MobileCapabilityType.DEVICE_NAME,"Test_Emulator1");
    		capabilities1.setCapability(MobileCapabilityType.APP,fs.getAbsolutePath()); 
    		capabilities1.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
    		System.out.println("Phone connected");
    		driver = new AndroidDriver<AndroidElement>(new URL("http://0.0.0.0:4723/wd/hub"), capabilities1);
    		System.out.println("App Launched");
    		Thread.sleep(500);
    		File file1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    		FileUtils.copyFile(file1, new File("Nero App Launched Successfully.jpg"));

    		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
            break;
          default:
        	  System.out.println("Invalid option choosen");
        	
        }
        
	}

}
