package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import prep.test.pages.Counter_Customer_Drop_Page;
import prep.test.pages.Counter_HomePage_Page;
import prep.test.pages.Counter_Location_Page;
import prep.test.pages.Counter_Location_Page1;

public class Counter_Customer_Drop_Steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_Customer_Drop_Page CounterCustDrop = new Counter_Customer_Drop_Page(driver);

	@Then("^Enter the WithEmailPopupbarcode and Digital Signature customer receipt with its value \"(.*)\"$")
	public void DropDomesticBarcodeWithEmailPopup(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropDomesticBarcodeWithEmailPopup(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.DropmailPopup();
		CounterCustDrop.DropCustomerReceipt();
		CounterCustDrop.DropBarcodeGreatJob(DBarcodeValue);
	}
	
	@Then("^Enter the print@office Yes barcode in Drop with its value \"(.*)\"$")
	public void DropbarcodewithprintatofficeYes(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropPrintatofficeYes(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.DropCustomerReceipt();
		CounterCustDrop.DropBarcodeGreatJob(DBarcodeValue);
	}
	
	@Then("^Enter the print@office No barcode in Drop with its value \"(.*)\"$")
	public void DropbarcodewithprintatofficeNo(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropPrintatofficeNo(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.DropCustomerReceipt();
		CounterCustDrop.DropBarcodeGreatJob(DBarcodeValue);
	}
	
	@Then("^Enter the ReturnParcelPrint Yes barcode in Drop with its value \"(.*)\"$")
	public void DropbarcodewithReturnParcelYes(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropPrintatofficeYes(DBarcodeValue);
		//CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		//CounterCustDrop.DropCustomerReceipt();
		//CounterCustDrop.DropBarcodeGreatJob(DBarcodeValue);
	}
	
	@Then("^Enter the ReturnParcelPrint No barcode in Drop with its value \"(.*)\"$")
	public void DropbarcodewithReturnParcelNo(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropPrintatofficeNo(DBarcodeValue);
		//CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		//CounterCustDrop.DropCustomerReceipt();
		//CounterCustDrop.DropBarcodeGreatJob(DBarcodeValue);
	}
	
	@Then("^Enter the WithoutEmailPopupbarcode with its value \"(.*)\"$")
	public void DropDomesticBarcodeWithoutEmail(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropDomesticBarcodeWithoutEmail(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.DropCustomerReceipt();
		CounterCustDrop.DropBarcodeGreatJob(DBarcodeValue);
	}
	
	@Then("^Enter the WithoutEmailPopupbarcode without login with its value \"(.*)\"$")
	public void DropDomesticBarcodeWithoutlogin(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropDomesticBarcodeWithoutlogin(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.DropCustomerReceipt();
		CounterCustDrop.DropBarcodeGreatJob(DBarcodeValue);
	}
	
	@Then("^Enter the Drop barcode without login and add to cart with its value \"(.*)\"$")
	public void BarcodeWithoutloginaddtocart(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropDomesticBarcodeWithoutlogin(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.DropAddmoreitem();
	}
	
	@Then("^Enter the barcode-Print receipt in EN without login with its value \"(.*)\"$")
	public void DropBarcodeWithoutloginPrintreceiptEN(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropDomesticBarcodeWithoutlogin(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.DropCustomerPrintReceipt();
		CounterCustDrop.DropBarcodeGreatJob(DBarcodeValue);
	}
	
	@Then("^Enter the barcode-No Cust receipt without login with its value \"(.*)\"$")
	public void DropBarcodeWithoutloginNocustreceiptEN(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropDomesticBarcodeWithoutlogin(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.DropNoCustomerReceipt();
		CounterCustDrop.DropBarcodeGreatJob(DBarcodeValue);
	}
	
	@Then("^Enter the International Checkin barcode Inside EU with its value \"(.*)\"$")
	public void DropinternationalbarcodeinsideEU(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropDomesticBarcodeWithoutEmail(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.Dropneedmoredetails(DBarcodeValue);
		CounterCustDrop.DropShipmentDetailsinsideEU();
		CounterCustDrop.Dropprohibitedcontent();
	}
	
	@Then("^Enter the International Checkin barcode Outside EU with its value \"(.*)\"$")
	public void DropinternationalbarcodeoutsideEU(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropDomesticBarcodeWithoutEmail(DBarcodeValue);
		CounterCustDrop.DropBarcodeSubmit(DBarcodeValue);
		CounterCustDrop.Dropneedmoredetails(DBarcodeValue);
		CounterCustDrop.DropShipmentDetailsoutsideEU();
		CounterCustDrop.Dropprohibitedcontent();
	}
	
	@When("Enter the Receipient details like {string}, {string},{string},{string},{string},{string},{string}")
	public void Enter_ReceipientDetailsInternational(String RecFirstName, String RecLastName,  String RecPostalCode,  String RecCity,  String RecAdress,  String streetnumber,  String RecEmail) throws Throwable {
		CounterCustDrop.EnterReceipientDetailsinternational(RecFirstName, RecLastName, RecPostalCode, RecCity, RecAdress, streetnumber, RecEmail);
	}
	
	@Then("^Enter the CheckinSender details like \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\" and \"(.*)\"$")
	public void Enter_SenderDetailsInternational(String SendFirstName, String SendLastName, String SendPostalCode, String SendStreet, String SendStreetNum, String SendEmail, String SendPhoneNum) throws Throwable {
		CounterCustDrop.EnterSenderCheckinDetails(SendFirstName, SendLastName, SendPostalCode, SendStreet, SendStreetNum, SendEmail, SendPhoneNum);
	}
	
	@Then("Select the Shipment type - Documents")
	public void SelectShipmentTypeDoc() throws Throwable {
		CounterCustDrop.DropShipemntTypeDocument();
	}
	
	@Then("Select the Shipment type - Gifts")
	public void SelectShipmentTypegifts() throws Throwable {
		CounterCustDrop.DropShipmenttypeGift();
	}
	
	@Then("Select the Shipment type - Sample")
	public void SelectShipmentTypeSample() throws Throwable {
		CounterCustDrop.DropShipmenttypeSample();
	}
	
	@Then("Select the Shipment type - Returned Goods")
	public void SelectShipmentTypeReturnedGoods() throws Throwable {
		CounterCustDrop.DropShipmenttypeRetGoods();
	}
	
	@Then("Select the Shipment type - Goods")
	public void SelectShipmentTypeGoods() throws Throwable {
		CounterCustDrop.DropShipmenttypeGoods();
	}
	
	@Then("Select the Shipment type - Other")
	public void SelectShipmentTypeOther() throws Throwable {
		CounterCustDrop.DropShipmenttypeOther();
	}
	
	@Then("Review Checkin details and Confirm Drop")
	public void ReviewCheckinDetailsbyUser() throws Throwable {
		CounterCustDrop.DropReviewCheckin();
		CounterCustDrop.ConfirmDropCheckin();
	}
	
	@Then("^Enter the print@office barcode in Drop - printer error with its value \"(.*)\"$")
	public void DropBarcodeprintatofficeprintererr(String DBarcodeValue) throws Throwable {
		CounterCustDrop.DropPrintatofficePrintererr(DBarcodeValue);
	}
	
	@When("^Validate wrong barcode in Drop \"(.*)\" scan with its error message \"(.*)\"$")
	public void ValidErrMessDrop(String DEBarcodeValue, String DEErrMsg) throws Throwable {
		CounterCustDrop.ValidErrMessDrop(DEBarcodeValue, DEErrMsg);
	}
}
