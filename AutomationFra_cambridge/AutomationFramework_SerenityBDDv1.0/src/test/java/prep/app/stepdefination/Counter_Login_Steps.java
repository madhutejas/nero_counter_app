package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import prep.test.pages.Counter_Login_Page;

public class Counter_Login_Steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_Login_Page LoginPage = new Counter_Login_Page(driver);

	@Then("Login")
	public void verifyCounterLogin() throws Throwable {
		LoginPage.verifyCounterLogin();
		
	}

}
