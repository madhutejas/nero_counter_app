package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import prep.test.pages.Counter_Location_Page;
import prep.test.pages.Counter_Location_Page1;

public class Counter_location_steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_Location_Page1 LocationPage1 = new Counter_Location_Page1(driver);

	@Then("SelectorConfirm Location1")
	public void verifyCounterLocation() throws Throwable {
		LocationPage1.verifyCounterLocation1();
	}
	
	@Then("SelectorConfirm Location_PP")
	public void verifyCounterLocationPP() throws Throwable {
		LocationPage1.verifyCounterLocation_PP();
	}
}
