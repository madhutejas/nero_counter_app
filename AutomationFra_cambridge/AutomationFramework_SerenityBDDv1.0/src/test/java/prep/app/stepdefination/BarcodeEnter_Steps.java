package prep.app.stepdefination;

import prep.test.pages.BarcodeEnter_Page;
import io.cucumber.java.en.*;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class BarcodeEnter_Steps {
	
	AndroidDriver<AndroidElement> driver;
	BarcodeEnter_Page BarcodeEnter = new BarcodeEnter_Page(driver);

	@When("^Enter the \"(.*)\" barcode with its value \"(.*)\"$")
	public void Enter_Barcode_for_scanning(String BarcodeType, String BarcodeValue) throws Throwable {
		BarcodeEnter.EnterBarcodeForScan(BarcodeType, BarcodeValue);
	}
	
	@When("^Validate wrong barcode \"(.*)\" scan with its error message \"(.*)\"$")
	public void Enter_WrongBarcode_for_scanning_ValidateErrMsg(String BarcodeValue, String ErrMsg) throws Throwable {
		BarcodeEnter.ValidErrMess(BarcodeValue, ErrMsg);
	}
	
}
