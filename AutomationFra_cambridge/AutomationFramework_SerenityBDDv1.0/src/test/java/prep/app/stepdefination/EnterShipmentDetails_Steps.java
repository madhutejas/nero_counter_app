package prep.app.stepdefination;

import prep.test.pages.EnterShipmentDetails_Page;

import io.cucumber.java.en.*;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class EnterShipmentDetails_Steps {

	AndroidDriver<AndroidElement> driver;
	EnterShipmentDetails_Page EnterSenderDetails = new EnterShipmentDetails_Page(driver);
	
	@Then("Select the Destination {string} Country")
	public void Select_Destination(String DestCountry) throws Throwable {
		EnterSenderDetails.SelectDestination(DestCountry);
	}
	
	@Then("If the Destination is {string} Country")
	public void Select_DestinationInOROutEU(String DestCountryInOROutEU) throws Throwable {
		EnterSenderDetails.SelectDestInOROutEU(DestCountryInOROutEU);
	}
	
	@Then("Agree the Prohibited and Restricted items")
	public void AgreeProhibitedAndRestItems() throws Throwable {
		EnterSenderDetails.AgreeProhibitedAndRestrictedItems();
	}
	
	@When("Enter the required Receipient details like {string}, {string},{string},{string},{string},{string},{string}")
	public void Enter_ReceipientDetails_for_shipping(String RecFirstName, String RecLastName,  String RecPostalCode,  String RecCity,  String RecAdress,  String RecEmail,  String RecPhoneNum) throws Throwable {
		EnterSenderDetails.EnterReceipientDetails(RecFirstName, RecLastName, RecPostalCode, RecCity, RecAdress, RecEmail, RecPhoneNum);
	}
	
//	@Then("Enter the required Sender details like {string},{string},{string},{string},{string},{string},{string}")
//	public void Enter_SenderDetails_for_shipping(String SendFirstName, String SendLastName,  String SendPostalCode,  String SendStreet,  String SendStreetNum,  String SendEmail,  String SendPhoneNum) throws Throwable {
//		EnterSenderDetails.EnterSenderDetails(SendFirstName, SendLastName, SendPostalCode, SendStreet, SendStreetNum, SendEmail, SendPhoneNum);
//	}
	
	@Then("Enter the required Sender details like {string}, {string},{string},{string},{string},{string},{string}")
	public void enter_the_required_Sender_details_like(String SendFirstName, String SendLastName, String SendPostalCode, String SendStreet, String SendStreetNum, String SendEmail, String SendPhoneNum) throws Throwable {
		EnterSenderDetails.EnterSenderDetails(SendFirstName, SendLastName, SendPostalCode, SendStreet, SendStreetNum, SendEmail, SendPhoneNum);
	}
	
	@Then("Validate Complete Details is Disabled")
	public void ValidateCompleteDetailsDisabled() throws Throwable {
		EnterSenderDetails.ValidateCompleteDetailsLink();
	}
	
	@Then("Validate Continue button disabled")
	public void ValidateContinueButDisable() throws Throwable {
		EnterSenderDetails.ValidateContinueButtonDisabled();
	}
	
	@Then("Click Continue")
	public void ClickContinue() throws Throwable {
		EnterSenderDetails.ClickContinue();
	}
	
	@Then("Validate msg if email id is not valid")
	public void ValidateMsg() throws Throwable {
		EnterSenderDetails.ValidateEmailValMsg();
	}
	
	@Then("Click Home")
	public void ClickHome() throws Throwable {
		EnterSenderDetails.ClickHome();
	}
	
	@Then("Select the Shipment Type as {string}")
	public void select_the_Shipment_Type_as(String ShipmentTypeIndex) throws Throwable {
	    EnterSenderDetails.SelectShipType(ShipmentTypeIndex);
	}
	
	@Then("Validate the popup message as {string}")
	public void Validate_the_popup_Message_as(String PopUpMsg) throws Throwable {
	    EnterSenderDetails.ValidatePopUp(PopUpMsg);
	}
	
	@Then("^Validate adress character as 40")
	public void Validate_the_adress_character() throws Throwable {
	    EnterSenderDetails.ValidateAdresChar();
	}
}
