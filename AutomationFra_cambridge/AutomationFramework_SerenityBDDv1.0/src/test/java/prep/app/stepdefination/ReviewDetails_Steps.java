package prep.app.stepdefination;

import prep.test.pages.ReviewDetails_Page;

import io.cucumber.java.en.*;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class ReviewDetails_Steps {
	
	AndroidDriver<AndroidElement> driver;
	ReviewDetails_Page ReviewDetails = new ReviewDetails_Page(driver);
	
	@Then("Choose click type Confirm OR Edit Details {string}")
	public void Click_TypeInReviewDetails(String ClickType) throws Throwable {
		ReviewDetails.ClickConfirmOREditDetails(ClickType);
	}
	
	@Then("Verify Checkin")
	public void Validate_CheckIn() throws Throwable {
		ReviewDetails.VerifyCheckIn();
	}
}
