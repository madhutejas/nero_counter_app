package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import prep.test.pages.Counter_Customer_Drop_Page;
import prep.test.pages.Counter_Customer_Intake_Page;
import prep.test.pages.Counter_Customer_Pickup_Page;
import prep.test.pages.Counter_Customer_RTS_Page;
import prep.test.pages.Counter_HomePage_Page;
import prep.test.pages.Counter_Location_Page;
import prep.test.pages.Counter_Location_Page1;

public class Counter_Customer_RTS_Steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_Customer_RTS_Page CounterCustRTS = new Counter_Customer_RTS_Page(driver);
	
	@Then("^Enter the barcode for RTS with value \"(.*)\"$")
	public void RTSBarcodescan(String RBarcodeValue) throws Throwable {
		CounterCustRTS.RTSBarcodescan(RBarcodeValue);
		CounterCustRTS.RTSMissingSkip();
		CounterCustRTS.RTSGreatJob();
	}
	
	@Then("^Mark barcode as missing with the barcode for RTS with RTSvalue \"(.*)\"$")
	public void RTSBarcodescanwithMissing(String RBarcodeValue) throws Throwable {
		CounterCustRTS.RTSBarcodescan(RBarcodeValue);
		CounterCustRTS.RTSMarkMissing();
		CounterCustRTS.RTSGreatJob();
	}
	
	@When("^Validate wrong barcode in RTS \"(.*)\" scan with its error message \"(.*)\"$")
	public void ValidErrMessRTS(String REBarcodeValue, String REErrMsg) throws Throwable {
		CounterCustRTS.ValidErrMessRTS(REBarcodeValue, REErrMsg);
	}
	
}
