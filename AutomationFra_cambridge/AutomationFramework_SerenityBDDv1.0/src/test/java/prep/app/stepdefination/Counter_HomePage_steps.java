package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import prep.test.pages.Counter_HomePage_Page;
import prep.test.pages.Counter_Location_Page;
import prep.test.pages.Counter_Location_Page1;

public class Counter_HomePage_steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_HomePage_Page CounterHomePage = new Counter_HomePage_Page(driver);

	@Then("VerifyHomePage")
	public void verifyCounterHomePage() throws Throwable {
		CounterHomePage.verifyCounterHomePage();
	}
}
