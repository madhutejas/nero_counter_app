package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import prep.test.pages.Counter_Customer_Drop_Page;
import prep.test.pages.Counter_Customer_Intake_Page;
import prep.test.pages.Counter_Customer_Pickup_Page;
import prep.test.pages.Counter_HomePage_Page;
import prep.test.pages.Counter_Location_Page;
import prep.test.pages.Counter_Location_Page1;
import prep.test.pages.Counter_Shop_Stockmngt_Page;

public class Counter_shop_StockMngt_Steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_Shop_Stockmngt_Page Countershopstockmngt = new Counter_Shop_Stockmngt_Page(driver);

	@Then("Check list of orders")
	public void OpenStockMngt() throws Throwable {
		Countershopstockmngt.StockMngtOpen();
	}
	
	@Then("Place order")
	public void StockMngtPlaceOrder() throws Throwable {
		Countershopstockmngt.StockMngtPlaceOrder();
		Countershopstockmngt.StockMngtReview();
		Countershopstockmngt.StockMngtGreatJob();
	}
	
	@Then("Check overview of inventory of sellables")
	public void StockOrderOverview() throws Throwable {
		Countershopstockmngt.StockMngtOpen();
		Countershopstockmngt.Orderoverview();
	}
}
