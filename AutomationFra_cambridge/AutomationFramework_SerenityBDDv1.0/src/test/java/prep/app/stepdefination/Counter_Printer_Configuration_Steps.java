package prep.app.stepdefination;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import prep.test.pages.Counter_Customer_Drop_Page;
import prep.test.pages.Counter_Customer_Pickup_Page;
import prep.test.pages.Counter_HomePage_Page;
import prep.test.pages.Counter_Location_Page;
import prep.test.pages.Counter_Location_Page1;
import prep.test.pages.Counter_Printer_Configuration_Page;

public class Counter_Printer_Configuration_Steps {
	
	AndroidDriver<AndroidElement> driver;
	Counter_Printer_Configuration_Page counterprinter = new Counter_Printer_Configuration_Page(driver);

	@Then("Select printers using printer configuration")
	public void Selectprintersforfranking() throws Throwable {
		counterprinter.SelectPrinter();
		counterprinter.SelectLabelPrinter();
		counterprinter.SelectReceiptPrinter();
		counterprinter.SelectPaymentTer();
		counterprinter.confirmprinter();
	}
	
	}
