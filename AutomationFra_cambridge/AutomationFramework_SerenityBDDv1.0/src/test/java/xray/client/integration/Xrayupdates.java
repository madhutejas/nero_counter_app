package xray.client.integration;

 

import java.io.File;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

 

import org.json.JSONException;

 

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

 

public class Xrayupdates {

    private static String FilePath;
    private static String endpoint;
    private static String Token;
    
     static EnvironmentVariables Prop = SystemEnvironmentVariables.createEnvironmentVariables();
     
     public static void UpdateXray() throws Exception {
         
    try {
         
         FilePath= Prop.getProperty("Reporting.path");
         
         File f = new File(FilePath);
         
         if(f.exists()){
             
         endpoint=Prop.getProperty("Reportingresult.Endpoint");
         Token=Prop.getProperty("Auth.Token");
         
         String ExpectedFile = readFileAsString(FilePath);
         
          URL url = new URL(endpoint);
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();

 

           conn.setRequestProperty("Authorization","Bearer "+ Token);
            //e.g. bearer token= eyJhbGciOiXXXzUxMiJ9.eyJzdWIiOiPyc2hhcm1hQHBsdW1zbGljZS5jb206OjE6OjkwIiwiZXhwIjoxNTM3MzQyNTIxLCJpYXQiOjE1MzY3Mzc3MjF9.O33zP2l_0eDNfcqSQz29jUGJC-_THYsXllrmkFnk85dNRbAw66dyEKBP5dVcFUuNTA8zhA83kk3Y41_qZYx43T

 

            conn.setRequestProperty("Content-Type","application/json");
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
        
            byte[] out = ExpectedFile.getBytes(StandardCharsets.UTF_8);

 

            OutputStream stream = conn.getOutputStream();
            stream.write(out);

 

            System.out.println(conn.getResponseCode() + " " + conn.getResponseMessage());
            conn.disconnect();
         }
         else {
             System.out.println("Cucumber file not generated,Please check your execution result");
             
         }
         }
            catch (Exception e) {
                  Throwable errorMessage = e.getCause();
                  System.out.println("GetParameters:"+ errorMessage);
                }
            
     }
     
     
     
       public static String readFileAsString(String file)throws Exception
        {
            return new String(Files.readAllBytes(Paths.get(file)));
        }

 

      //Main method
       public static void main(String[] args) throws Exception {
           
           UpdateXray();
           
       }
       
    
}