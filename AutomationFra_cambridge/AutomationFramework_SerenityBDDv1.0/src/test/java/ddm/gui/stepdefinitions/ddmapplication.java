package ddm.gui.stepdefinitions;

import java.io.IOException;

import ddm.gui.homepage.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ddmapplication {
	
	@Steps
	HomePage LoginPage;
	
	@Given("Admin user is on Birdy home page")
	public void adminUserIsOnBirdyHomePage() {
	    // Write code here that turns the phrase above into concrete actions
		
		LoginPage.OpenBrowser();
	  
	}

	@When("Admin user enter username")
	public void adminUserEnterUsername() {
	    // Write code here that turns the phrase above into concrete actions
		
		LoginPage.enterusername();
	   
	}

	@When("Admin user enter password")
	public void adminUserEnterPassword() {
	    // Write code here that turns the phrase above into concrete actions
		
		LoginPage.enterpassword();
	  
	}

	@When("Admin click login button")
	public void adminClickLoginButton() {
	    // Write code here that turns the phrase above into concrete actions
	  
		LoginPage.loginbutton();
	}

	@Then("User should redirected to Demand dashboard screen")
	public void userShouldRedirectedToDemandDashboardScreen() {
	    // Write code here that turns the phrase above into concrete actions
		
		LoginPage.verifyPage();
	 
	}

}
