package ddm.gui.homepage;

import net.thucydides.core.annotations.Step;

public class HomePage {
	
	HomePageObjects HomeObject;
	
	@Step
	public void OpenBrowser() {
		
		HomeObject.open();
	}
	@Step
	public void enterusername() {
		
		HomeObject.ddm_username();
	}
	@Step
	public void enterpassword() {
		
		HomeObject.ddm__password();
	}
	@Step
	public void loginbutton() {
		
		HomeObject.ddm_loginclick();
		
	}
	@Step
    public void verifyPage() {
    	
    	HomeObject.ddm_demandscreen();
    	
    }


}





