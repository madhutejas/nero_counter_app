package ddm.gui.runner.core;
import io.cucumber.core.snippets.SnippetType;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

import org.aspectj.lang.annotation.After;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		plugin = {"pretty","json:target/cucumber-reports/Cucumber.json"},
        features = "src/test/resources/features",
        glue = {"prep.app.stepdefination"},
        tags = {"@TEST_NRO-4366"},
        strict = true,
        snippets= io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE)
public class DDMTestSuite {

    @AfterClass
    public static void teardown() {
        System.out.println("Ran the after");
    }
}

